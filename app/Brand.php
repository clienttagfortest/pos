<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $connection = 'mysql';
    protected $table = 'brands';
    protected $fillable = ['brand_code', 'name', 'category_id', 'company_id', 'image'];
}
