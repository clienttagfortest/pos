@extends('layouts.master')
@section('title', 'Role')
@section('content')

        <div class="row page-titles">
            <div class="col-md-5 align-self-center mb-2">
                <h2 class="text-themecolor">สร้างสิทธิ์การใช้งาน</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('setting.role.index') }}">สิทธิ์การใช้งาน</a></li>
                    <li class="breadcrumb-item active">สร้างสิทธิ์การใช้งาน</li>
                </ol>
            </div>
        </div>

		<div class="card card-form">
			{!! Form::open(['route' => 'setting.role.store', 'method' => 'post']) !!}

			<div class="form-group">
				{!! Form::label('name', 'ชื่อ') !!}
				{!! Form::text('name', null, ['class' => 'form-control']) !!}
			</div>

			<div class="form-group">
				<div class="form-flex">
					<div class="form-label">
						{!! Form::label('sell', 'รายการขาย') !!}
					</div>
					<div class="form-input">
						<label><input type="radio" name="sell" value="not" checked>ไม่มี</label>
						<label><input type="radio" name="sell" value="all">ทั้งหมด</label>
						<label><input type="radio" name="sell" value="read">ดูเท่านั้น</label>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-flex">
					<div class="form-label">
						{!! Form::label('sale', 'รายการซื้อ') !!}
					</div>
					<div class="form-input">
						<label><input type="radio" name="buy" value="not" checked>ไม่มี</label>
						<label><input type="radio" name="buy" value="all">ทั้งหมด</label>
						<label><input type="radio" name="buy" value="read">ดูเท่านั้น</label>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-flex">
					<div class="form-label">
						{!! Form::label('product', 'รายการสินค้า') !!}
					</div>
					<div class="form-input">
						<label><input type="radio" name="product" value="not" checked>ไม่มี</label>
						<label><input type="radio" name="product" value="all">ทั้งหมด</label>
						<label><input type="radio" name="product" value="read">ดูเท่านั้น</label>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-flex">
					<div class="form-label">
						{!! Form::label('brand', 'ยี่ห้อสินค้า') !!}
					</div>
					<div class="form-input">
						<label><input type="radio" name="brand" value="not" checked>ไม่มี</label>
						<label><input type="radio" name="brand" value="all">ทั้งหมด</label>
						<label><input type="radio" name="brand" value="read">ดูเท่านั้น</label>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-flex">
					<div class="form-label">
						{!! Form::label('warehouse', 'คลังสินค้า') !!}
					</div>
					<div class="form-input">
						<label><input type="radio" name="warehouse" value="not" checked>ไม่มี</label>
						<label><input type="radio" name="warehouse" value="all">ทั้งหมด</label>
						<label><input type="radio" name="warehouse" value="read">ดูเท่านั้น</label>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-flex">
					<div class="form-label">
						{!! Form::label('customer', 'ลูกค้า') !!}
					</div>
					<div class="form-input">
						<label><input type="radio" name="customer" value="not" checked>ไม่มี</label>
						<label><input type="radio" name="customer" value="all">ทั้งหมด</label>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-flex">
					<div class="form-label">
						{!! Form::label('finance', 'การเงิน') !!}
					</div>
					<div class="form-input">
						<label><input type="radio" name="finance" value="not" checked>ไม่มี</label>
						<label><input type="radio" name="finance" value="all">ทั้งหมด</label>
						<label><input type="radio" name="finance" value="income">รายรับ</label>
						<label><input type="radio" name="finance" value="cost">รายจ่าย</label>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-flex">
					<div class="form-label">
						{!! Form::label('setting', 'ตั้งค่า') !!}
					</div>
					<div class="form-input">
						<label><input type="radio" name="setting" value="not" checked>ไม่มี</label>
						<label><input type="radio" name="setting" value="all">ทั้งหมด</label>
						<label><input type="radio" name="setting" value="business">ข้อมูลธุรกิจ</label>
						<label><input type="radio" name="setting" value="user">ผู้ใช้งาน</label>
						<label><input type="radio" name="setting" value="role">สิทธิ์การใช้งาน</label>
						<label><input type="radio" name="setting" value="noti">การแจ้งเตือน</label>
					</div>
				</div>
			</div>

			<div class="form-group">
				{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
			</div>

			{!! Form::close() !!}
		</div>

@stop
