<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $connection = 'mysql';
    protected $table = 'customers';
    protected $fillable = ['name', 'customer_code', 'tel', 'email', 'address', 'company_id'];
}
