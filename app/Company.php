<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $connection = 'mysql';
    protected $table = 'companies';
    protected $fillable = ['name', 'tax_numbar', 'tel', 'website', 'email', 'address', 'code'];
}
