<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123123123'),
            'company_id' => null,
            'status' => true,
            // 'role_id' => 'root',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
