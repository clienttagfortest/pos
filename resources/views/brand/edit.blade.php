@extends('layouts.master')
@section('title', 'Brand')
@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center mb-2">
            <h2 class="text-themecolor">แก้ไขแบรนด์</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
                <li class="breadcrumb-item"><a href="{{ route('brand.index') }}">แบรนด์</a></li>
                <li class="breadcrumb-item active">แก้ไขแบรนด์</li>
            </ol>
        </div>
    </div>

    <!-- Default box -->
    <div class="card">

        <div class="card-body">

        {!! Form::model($brand , ['route' => ['brand.update', $brand->id], 'method' => 'patch', 'files' => true]) !!}

        <div class="form-group">
            {!! Form::label('name', 'ชื่อแบรนด์') !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('category_id', 'หมวดหมู่') !!}
            {!! Form::select('category_id', $categories, null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('image', 'รูป') !!}
            {!! Form::file('image', ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
        </div>

        {!! Form::close() !!}

        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

@stop
