@extends('layouts.master')
@section('title', 'Role')
@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center mb-2">
            <h2 class="text-themecolor">แก้ไขสิทธิ์การใช้งาน</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
                <li class="breadcrumb-item"><a href="{{ route('setting.role.index') }}">สิทธิ์การใช้งาน</a></li>
                <li class="breadcrumb-item active">แก้ไขสิทธิ์การใช้งาน</li>
            </ol>
        </div>
    </div>

    <div class="card card-form">
        {!! Form::model($role, ['route' => ['setting.role.update', $role->id], 'method' => 'patch']) !!}

        <div class="form-group">
            {!! Form::label('name', 'ชื่อ') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            <div class="form-flex">
                <div class="form-label">
                    {!! Form::label('sell', 'รายการขาย') !!}
                </div>
                <div class="form-input">
                    <label><input type="radio" name="sell" value="not" @if($role->sell == 'not') checked @endif>ไม่มี</label>
                    <label><input type="radio" name="sell" value="all" @if($role->sell == 'all') checked @endif>ทั้งหมด</label>
                    <label><input type="radio" name="sell" value="read" @if($role->sell == 'read') checked @endif>ดูเท่านั้น</label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="form-flex">
                <div class="form-label">
                    {!! Form::label('sale', 'รายการซื้อ') !!}
                </div>
                <div class="form-input">
                    <label><input type="radio" name="buy" value="not" @if($role->buy == 'not') checked @endif>ไม่มี</label>
                    <label><input type="radio" name="buy" value="all" @if($role->buy == 'all') checked @endif>ทั้งหมด</label>
                    <label><input type="radio" name="buy" value="read" @if($role->buy == 'read') checked @endif>ดูเท่านั้น</label>
                </div>
            </div>
        </div>



        <div class="form-group">
            <div class="form-flex">
                <div class="form-label">
                    {!! Form::label('warehouse', 'คลังสินค้า') !!}
                </div>
                <div class="form-input">
                    <label><input type="radio" name="warehouse" value="not" @if($role->warehouse == 'not') checked @endif>ไม่มี</label>
                    <label><input type="radio" name="warehouse" value="all" @if($role->warehouse == 'all') checked @endif>ทั้งหมด</label>
                    <label><input type="radio" name="warehouse" value="read" @if($role->warehouse == 'read') checked @endif>ดูเท่านั้น</label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="form-flex">
                <div class="form-label">
                    {!! Form::label('product', 'รายการสินค้า') !!}
                </div>
                <div class="form-input">
                    <label><input type="radio" name="product" value="not" @if($role->product == 'not') checked @endif>ไม่มี</label>
                    <label><input type="radio" name="product" value="all" @if($role->product == 'all') checked @endif>ทั้งหมด</label>
                    <label><input type="radio" name="product" value="read" @if($role->product == 'read') checked @endif>ดูเท่านั้น</label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="form-flex">
                <div class="form-label">
                    {!! Form::label('brand', 'ยี่ห้อสินค้า') !!}
                </div>
                <div class="form-input">
                    <label><input type="radio" name="brand" value="not" @if($role->brand == 'not') checked @endif>ไม่มี</label>
                    <label><input type="radio" name="brand" value="all" @if($role->brand == 'all') checked @endif>ทั้งหมด</label>
                    <label><input type="radio" name="brand" value="read" @if($role->brand == 'read') checked @endif>ดูเท่านั้น</label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="form-flex">
                <div class="form-label">
                    {!! Form::label('customer', 'ลูกค้า') !!}
                </div>
                <div class="form-input">
                    <label><input type="radio" name="customer" value="not" @if($role->customer == 'not') checked @endif>ไม่มี</label>
                    <label><input type="radio" name="customer" value="all" @if($role->customer == 'all') checked @endif>ทั้งหมด</label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="form-flex">
                <div class="form-label">
                    {!! Form::label('finance', 'การเงิน') !!}
                </div>
                <div class="form-input">
                    <label><input type="radio" name="finance" value="not" @if($role->finance == 'not') checked @endif>ไม่มี</label>
                    <label><input type="radio" name="finance" value="all" @if($role->finance == 'all') checked @endif>ทั้งหมด</label>
                    <label><input type="radio" name="finance" value="income" @if($role->finance == 'income') checked @endif>รายรับ</label>
                    <label><input type="radio" name="finance" value="cost" @if($role->finance == 'cost') checked @endif>รายจ่าย</label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="form-flex">
                <div class="form-label">
                    {!! Form::label('setting', 'ตั้งค่า') !!}
                </div>
                <div class="form-input">
                    <label><input type="radio" name="setting" value="not" @if($role->setting == 'not') checked @endif>ไม่มี</label>
                    <label><input type="radio" name="setting" value="all" @if($role->setting == 'all') checked @endif>ทั้งหมด</label>
                    <label><input type="radio" name="setting" value="business" @if($role->setting == 'business') checked @endif>ข้อมูลธุรกิจ</label>
                    <label><input type="radio" name="setting" value="user" @if($role->setting == 'user') checked @endif>ผู้ใช้งาน</label>
                    <label><input type="radio" name="setting" value="role" @if($role->setting == 'role') checked @endif>สิทธิ์การใช้งาน</label>
                    <label><input type="radio" name="setting" value="noti" @if($role->setting == 'noti') checked @endif>การแจ้งเตือน</label>
                </div>
            </div>
        </div>

        <div class="form-group">
            {!! Form::submit('ยืนยัน', ['class' => 'btn btn-primary']) !!}
        </div>

        {!! Form::close() !!}
    </div>

@stop
