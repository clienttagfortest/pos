<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuyRecord extends Model
{
    protected $connection = 'mysql';
    protected $table = 'buy_records';
    protected $fillable = ['buy_record_code', 'date', 'tax_type', 'partner_id', 'result_price', 'company_id'];

    public function partner()
    {
    	return  $this->belongsTo('App\Partner');
    }

    public function buy_record_products()
    {
    	return  $this->hasMany('App\BuyRecordProduct', 'buy_record_code', 'buy_record_code');
    }
}
