<?php

namespace App\Imports;

use App\SellRecordProduct;
use App\Company;
use Maatwebsite\Excel\Concerns\ToModel;

class SellRecordImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new SellRecordProduct([
            'id' => $row[0],
            'sell_record_code' => $row[1],
            'product_code' => $row[2],
            'name' => $row[3],
            'amount' => $row[4],
            'unit_price' => $row[5],
            'discount_unit_price' => $row[6],
            'total_price' => $row[7],
            'name' => $row[7],
            'date' => $row[8]

        ]);
    }
}
