<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellRecord extends Model
{
    protected $connection = 'mysql';
    protected $table = 'sell_records';
    protected $fillable = ['sell_record_code', 'date', 'tax_type', 'customer_id', 'result_price', 'company_id'];

    public function customer()
    {
    	return $this->belongsTo('App\Customer');
    }

    public function sell_record_products()
    {
    	return  $this->hasMany('App\SellRecordProduct', 'sell_record_code', 'sell_record_code');
    }

}
