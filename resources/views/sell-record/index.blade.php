@extends('layouts.master')
@section('title', 'Sell Record')
@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center mb-2">
            <h2 class="text-themecolor">รายการขาย</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
                <li class="breadcrumb-item active">รายการขาย</li>
            </ol>
        </div>
        <div class="col-sm-7 align-self-center">
            <a href="{{ route('sell-record.create') }}" class="btn btn-info float-right" >+ สร้างรายการขาย</a>
        </div>
    </div>

    <!-- Default box -->
    <div class="card">
        <!-- /.card-header -->
        <div class="card-body">

            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>รายการ</th>
                        <th>วันทำรายการ</th>
                        <th>ลูกค้า</th>
                        <th>ยอดรวม</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sell_records as $key => $sell_record)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $sell_record->sell_record_code }}</td>
                        <td>{{ $sell_record->date }}</td>
                        <td>{{ $sell_record->customer->name }}</td>
                        <td>{{ $sell_record->result_price }}</td>
                        <td>
                            <a href="{{ route('sell-record.show', $sell_record->id) }}" class="btn btn-sm btn-info">ดูข้อมูล</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <div class="card-body">
                        <form action="{{ route('Sellimport') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <a class="btn btn-warning" href="{{ route('Sellexport') }}">Export All Rec Data</a>
                        </form>

                        <div class="input-group" style=" width:500px; float:right; ">
                            <span class="input-group-text">Date</span>
                            <input style=" text-align: center;" type="text" placeholder="Start date" aria-label="First name"
                                class="form-control start-date">
                            <input style=" text-align: center;" type="text" placeholder="End date" aria-label="Last name"
                                class="form-control end-date">
                            <button type="button" class="btn btn-outline-secondary docs-datepicker-trigger" disabled>
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </button>
                        </div>

                    </div>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

@stop
