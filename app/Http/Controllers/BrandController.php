<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Brand;
use App\Category;
use Storage;

class BrandController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'login', 'CheckBrand']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_auth = Auth::user();

        $brands = Brand::where('company_id', $user_auth->company_id)->get();


        return view('brand.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id');
        return view('brand.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_auth = Auth::user();

        $input = $request->input();
        $brand_code = $this->generateCustomerCode();
        $input['company_id'] = $user_auth->company_id;
        $input['brand_code'] = $brand_code;

        if ($request->hasFile('image')) {
            $image_path = 'uploads/'.$request->file('image')->hashName();
            $s3 = Storage::disk('s3');
            $s3->put($image_path, file_get_contents($request->file('image')), 'public');
            $input['image'] = $image_path;
        }

        Brand::create($input);
        return redirect()->route('brand.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_auth = Auth::user();

        $brand = Brand::where('company_id', $user_auth->company_id)->where('id', $id)->firstOrFail();
        $categories = Category::pluck('name', 'id');

        return view('brand.edit', compact('brand', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_auth = Auth::user();
        $input = $request->input();

        $brand = Brand::where('company_id', $user_auth->company_id)->where('id', $id)->firstOrFail();

        if ($request->hasFile('image')) {
            $s3 = Storage::disk('s3');
            $s3->delete($brand->image);

            $image_path = 'uploads/'.$request->file('image')->hashName();
            $s3->put($image_path, file_get_contents($request->file('image')), 'public');

            $input['image'] = $image_path;
        }

        $brand->update($input);
        return redirect()->route('brand.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_auth = Auth::user();

        $brand = Brand::where('company_id', $user_auth->company_id)->where('id', $id)->firstOrFail();
        $brand->delete();
        $s3 = Storage::disk('s3');
        $s3->delete($brand->image);
        return redirect()->route('brand.index');
    }


    public function generateCustomerCode()
    {
        $user_auth = Auth::user();
        $company_code = $user_auth->company->code;
        $customer_code = $company_code.'B';
        $alphabet = '0123456789';
        for ($i=0; $i < 8; $i++) {
            $rand = rand(0, strlen($alphabet)-1);
            $customer_code = $customer_code.$alphabet[$rand];
        }
        return $customer_code;
    }

}
