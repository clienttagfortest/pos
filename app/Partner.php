<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $connection = 'mysql';
    protected $table = 'partners';
    protected $fillable = ['name', 'partner_code', 'tel', 'email', 'address', 'company_id'];
}
