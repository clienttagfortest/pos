<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Customer;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'login', 'CheckCustomer']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_auth = Auth::user();
        
        $customers = Customer::where('company_id', $user_auth->company_id)->get();
        return view('customer.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_auth = Auth::user();

        $input = $request->input();
        $customer_code = $this->generateCustomerCode();
        $input['customer_code'] = $customer_code;
        $input['company_id'] = $user_auth->company_id;
        Customer::create($input);
        return redirect()->route('customer.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_auth = Auth::user();

        $customer = Customer::where('company_id', $user_auth->company_id)->where('id', $id)->firstOrFail();
        return view('customer.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_auth = Auth::user();

        $input = $request->input();
        $customer = Customer::where('company_id', $user_auth->company_id)->where('id', $id)->firstOrFail();
        $customer->update($input);
        return redirect()->route('customer.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_auth = Auth::user();

        $customer = Customer::where('company_id', $user_auth->company_id)->where('id', $id)->firstOrFail();
        $customer->delete();
        return redirect()->route('customer.index');
    }

    public function generateCustomerCode()
    {
        $user_auth = Auth::user();
        $company_code = $user_auth->company->code;
        $customer_code = $company_code.'C';
        $alphabet = '0123456789';
        for ($i=0; $i < 8; $i++) { 
            $rand = rand(0, strlen($alphabet)-1);
            $customer_code = $customer_code.$alphabet[$rand];
        }
        return $customer_code;
    }
}
