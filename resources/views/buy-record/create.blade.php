@extends('layouts.master')
@section('title', 'Buy Record')
@section('content')

    <div class="row page-titles mb-1">
        <div class="col-md-5 align-self-center">
            <h2 class="text-themecolor">สร้างรายการซื้อ</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
                <li class="breadcrumb-item"><a href="{{ route('buy-record.index') }}">รายการซื้อ</a></li>
                <li class="breadcrumb-item active">สร้างรายการซื้อ</li>
            </ol>
        </div>
    </div>

    {!! Form::open(['route' => 'buy-record.store', 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-6">
            <hr class="mt-1">
            <div class="content">

                <div class="form-group row">
                    {!! Form::label('buy_record_code', 'เลขที่รายการ', ['class' => 'col-3 control-label text-right']) !!}
                    <div class="col-md-8 col-9">
                        {!! Form::text('buy_record_code', $buy_code, ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>

                <div class="form-group row">
                    {!! Form::label('date', 'วันที่ทำรายการ', ['class' => 'col-3 control-label text-right']) !!}
                    <div class="col-md-8 col-9">
                        {!! Form::date('date', null, ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>

                {{-- <div class="form-group">
                    {!! Form::label('tax_type', 'ประเภทภาษี') !!}
                    {!! Form::select('tax_type', ['not' => 'ไม่มีภาษี', 'exclude' => 'ไม่รวมภาษี', 'include' => 'รวมภาษี'], null, ['class' => 'form-control']) !!}
                </div> --}}
            </div>
        </div>
        <div class="col-md-6">
            <hr class="mt-1">
            <div class="content">
                <input type="hidden" name="partner[partner_id]" id="partner-id">
                <h3 class="card-title d-inline-block">คู่ค้า</h3>
                <a onclick="findPartner()">
                    <i class="fa fa-search"></i>
                </a>

                <div class="form-group row">
                    <label for="partner[name]" class="control-label text-right col-3">ชื่อ</label>
                    <div class="col-md-8 col-9">
                        <input type="text" name="partner[name]" class="form-control" id="partner-name" readonly>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="partner[tel]" class="control-label text-right col-3">เบอร์มือถือ</label>
                    <div class="col-md-8 col-9">
                        <input type="text" name="partner[tel]" class="form-control" id="partner-tel" readonly>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="partner[email]" class="control-label text-right col-3">อีเมล</label>
                    <div class="col-md-8 col-9">
                        <input type="email" name="partner[email]" class="form-control" id="partner-email" readonly>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="partner[address]" class="control-label text-right col-3">ที่อยู่</label>
                    <div class="col-md-8 col-9">
                        <textarea name="partner[address]" class="form-control" id="partner-address" rows="3" required></textarea>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-sm-12">
            <div class="card card-form">

                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>#</th>
                            {{-- <th>รหัส</th> --}}
                            <th>ชื่อสินค้า</th>
                            <th>จำนวน</th>
                            <th>มูลค่าต่อหน่วย</th>
                            <th>ส่วนลดต่อหน่วย</th>
                            <th>รวม</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="list-product">
                        <tr>
                            <input type="hidden" id="product_code_1" name="product_code[]" class="form-control">
                            <td>
                                <a onclick="findProduct(1)"><i class="fas fa-search"></i></a>
                            </td>
                            <td>
                                1
                            </td>
                            <td class="p-1">
                                <input type="text" id="name_1" name="name[]" class="form-control" readonly>
                            </td>
                            <td class="p-1">
                                <input type="number" id="amount_1" name="amount[]" onchange="calPrice(1)" class="form-control" required>
                            </td>
                            <td class="p-1">
                                <input type="number" id="unit_price_1" name="unit_price[]" onchange="calPrice(1)" class="form-control" required>
                            </td>
                            <td class="p-1">
                                <input type="number" id="discount_unit_price_1" name="discount_unit_price[]" onchange="calPrice(1)" class="form-control" required>
                            </td>
                            <td class="p-1">
                                <input type="text" name="total_price[]" class="form-control" id="total-price-1" readonly>
                            </td>
                            <td class="p-1">

                            </td>
                        </tr>
                    </tbody>
                </table>
                <a onclick="addProduct()" id="btn-add-product">+ เพิ่มสินค้า</a>
                <div class="result-box">
                    <div class="row">
                        <div class="col-md-6">

                        </div>
                        <div class="col-sm-3">
                            มูลค่ารวม
                        </div>
                        <div class="col-sm-3">
                            <div class="result-price"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group align-center">
        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}

	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">เลือกสินค้า</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $key => $product)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $product->name }}</td>
                                <td>
                                    <a onclick="chooseProduct({{ $product }})">เลือก</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
		</div>
	</div>

	<div class="modal fade" id="partnerModal" role="dialog">
		<div class="modal-dialog">
		  <!-- Modal content-->
		  <div class="modal-content">
		    <div class="modal-header">
		    	<h4 class="modal-title">เลือกคู่ค้า</h4>
		      	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    </div>
		    <div class="modal-body">
		      	<table class="table">
		      		<thead>
		      			<tr>
		      				<th>#</th>
		      				<th>Name</th>
		      				<th></th>
		      			</tr>
		      		</thead>
		      		<tbody>
		      			@foreach($partners as $key => $partner)
		      			<tr>
		      				<td>{{ ++$key }}</td>
		      				<td>{{ $partner->name }}</td>
		      				<td>
		      					<a onclick="choosePartner({{ $partner }})">เลือก</a>
		      				</td>
		      			</tr>
		      			@endforeach
		      		</tbody>
		      	</table>
		    </div>
		    <div class="modal-footer">
		      	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    </div>
		  </div>
		</div>
	</div>

	<script>

		// setDateInput();
		function setDateInput() {
			let n = new Date();
			let d = n.getDate();
			let m = n.getMonth()+1;
			let y = n.getFullYear();
			if (d < 10) { d = '0'+d; }
			if (m < 10) { m = '0'+m; }
			let date = y+'-'+m+'-'+d
			$('#input-date').val(date)
		}

		let choose_row = '';
		let result_arr = [];
		function addProduct() {
			let count = $('.list-product > tr').length;
			let num_row = count+1;

			$('.list-product').append('<tr id="row-'+ num_row +'" data-row="'+ num_row +'"><input type="hidden" id="product_code_'+ num_row +'" name="product_code[]" class="form-control"><td><a onclick="findProduct('+ num_row +')"><i class="fas fa-search"></i></a></td><td class="index">'+ num_row +'</td><td><input type="text" id="name_'+ num_row +'" name="name[]" class="form-control" readonly></td><td><input type="text" id="amount_'+ num_row +'" name="amount[]" onchange="calPrice('+ num_row +')" class="form-control"></td><td><input type="text" id="unit_price_'+ num_row +'" name="unit_price[]" onchange="calPrice('+ num_row +')" class="form-control"></td><td><input type="text" id="discount_unit_price_'+ num_row +'" name="discount_unit_price[]" onchange="calPrice('+ num_row +')" class="form-control"></td><td><input type="text" name="total_price[]" class="form-control" id="total-price-'+ num_row +'" readonly></td><td><a onclick="removeProduct('+ num_row +')"><i class="fa fa-times"></i></a></td></tr>');
		}


		function removeProduct(num_row) {
			$('#row-'+num_row).remove();
		}

		function findProduct(num_row) {
			choose_row = num_row;
			$('#myModal').modal('show');
		}

		function chooseProduct(product) {
			$('#product_code_'+choose_row).val(product.product_code);
			$('#name_'+choose_row).val(product.name);
			$('#amount_'+choose_row).val(1);
			$('#unit_price_'+choose_row).val(product.purchase_price);

			let total_price = product.purchase_price*1;
			$('#total-price-'+choose_row).val(total_price);
			$('#myModal').modal('hide');

		}


		function calPrice(num_row) {
			let amount = $('#amount_'+num_row).val();
			let unit_price = $('#unit_price_'+num_row).val();
			let discount_unit_price = $('#discount_unit_price_'+num_row).val();
			if (discount_unit_price == '') { discount_unit_price == 0 }

			let total_price = amount * (unit_price - discount_unit_price);
			$('#total-price-'+num_row).val(total_price);

		}


		function findPartner() {
			$('#partnerModal').modal('show');
		}

		function choosePartner(partner) {
			let partner_id = $('#partner-id').val(partner.id);
			let name = $('#partner-name').val(partner.name);
			let tel = $('#partner-tel').val(partner.tel);
			let email = $('#partner-email').val(partner.email);
			let address = $('#partner-address').val(partner.address);
			$('#partnerModal').modal('hide');

		}

	</script>

@stop

