@extends('layouts.master')
@section('title', 'User')
@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center mb-2">
            <h2 class="text-themecolor">ผู้ใช้งาน</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
                <li class="breadcrumb-item active">ผู้ใช้งาน</li>
            </ol>
        </div>
        <div class="col-sm-7 align-self-center">
            <a href="{{ route('setting.user.create') }}" class="btn btn-info float-right" >+ เพิ่มผู้ใช้</a>
        </div>
    </div>

    <div class="card">
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ชื่อผู้ใช้งาน</th>
                    <th>วันที่สมัคร</th>
                    <th>สิทธิ์ใช้งาน</th>
                    <th>สถานะ</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $key => $user)
                <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->created_at }}</td>
                    <td>{{ $user->role->name }}</td>
                    <td>{{ $user->status }}</td>
                    <td>
                        <a href="{{ route('setting.user.edit', $user->id) }}" class="btn btn-sm btn-warning">แก้ไข</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@stop
