<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SellRecord;
use App\BuyRecord;
use App\Product;
use App\SellRecordProduct;
use App\BuyRecordProduct;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'login']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        $user_auth = Auth::user();

        $sales = SellRecord::where('company_id', $user_auth->company_id)->get();
        $buys = BuyRecord::where('company_id', $user_auth->company_id)->get();
        // $sales = SellRecordProduct::where('company_id', $user_auth->company_id)->get();
        // $buys = BuyRecordProduct::where('company_id', $user_auth->company_id)->get();
        // $sales_product = SellRecordProduct::where('company_id', $user_auth->company_id)->get();
        // $buys_product = BuyRecordProduct::where('company_id', $user_auth->company_id)->get();
        $sum_sale = intval(SellRecord::where('company_id', $user_auth->company_id)->sum('result_price'));
        $sum_buy  = intval(BuyRecord::where('company_id', $user_auth->company_id)->sum('result_price'));
        $sum_buy_amount  = intval(BuyRecordProduct::where('company_id', $user_auth->company_id)->sum('amount'));
        $sum_sell_amout  = intval(SellRecordProduct::where('company_id', $user_auth->company_id)->sum('amount'));
        $sum_buy_product = intval(Product::where('company_id', $user_auth->company_id)->sum('purchase_price'));
        $sum_sell_product = intval(Product::where('company_id', $user_auth->company_id)->sum('sale_price'));
        $real_profit = ($sum_sell_product*$sum_sell_amout) - ($sum_buy_product*$sum_sell_amout);
        $profit = $sum_sale - $sum_buy;
        return view('dashboard',compact('sales', 'buys', 'sum_sale', 'sum_buy', 'profit','sum_buy_product','sum_sell_product', 'real_profit'));
    }

}
