<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\BuyRecordExport;
use App\Imports\BuyRecordImport;
use Maatwebsite\Excel\Facades\Excel;

class MyControllerBuy extends Controller
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function importExportView()
    {
       return view('import');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export()
    {
        // dd(carbon::now(get_local_time()));
        // $table->dateTime('created_at')->nullable()->default(new DateTime());
        return Excel::download(new BuyRecordExport, 'BuyRecord-'. date('Y-m-d ') .'Time'.date(' H-i-s').'.xlsx');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function import()
    {
        Excel::import(new SellRecordImport,request()->file('file'));

        return back();
    }
}
