<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $connection = 'mysql';
    protected $table = 'roles';
    protected $fillable = ['name', 'sell', 'buy', 'product', 'brand', 'warehouse', 'customer', 'finance', 'setting', 'company_id'];
}
