@extends('layouts.master')
@section('title', 'Role')
@section('content')

        <div class="row page-titles">
            <div class="col-md-5 align-self-center mb-2">
                <h2 class="text-themecolor">สิทธิ์การใช้งาน</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
                    <li class="breadcrumb-item active">สิทธิ์การใช้งาน</li>
                </ol>
            </div>
            <div class="col-sm-7 align-self-center">
                <a href="{{ route('setting.role.create') }}" class="btn btn-info float-right" >+ เพิ่มสิทธิ์การใช้งาน</a>
            </div>
        </div>

		<div class="card">
			<table class="table">
				<thead>
					<tr>
						<th width="10%">#</th>
						<th width="70%">ชื่อสิทธิ์การใช้งาน</th>
						<th width="20%"></th>
					</tr>
				</thead>
				<tbody>
					@foreach($roles as $key => $role)
					<tr>
						<td>{{ ++$key }}</td>
						<td>{{ $role->name }}</td>
						<td>
							<a href="{{ route('setting.role.edit', $role->id) }}" class="btn btn-sm btn-warning">แก้ไข</a>
							{!! Form::open(['route' => ['setting.role.destroy', $role->id], 'method' => 'delete', 'class' => 'btn-del']) !!}
							{!! Form::submit('ลบ', ['class' => 'btn btn-sm btn-danger']) !!}
							{!! Form::close() !!}
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>

@stop
