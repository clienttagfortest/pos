@extends('layouts.master')
@section('title', 'Partner')
@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center mb-2">
            <h2 class="text-themecolor">รายชื่อคู่ค้า</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
                <li class="breadcrumb-item active">รายชื่อคู่ค้า</li>
            </ol>
        </div>
        <div class="col-sm-7 align-self-center">
            <a href="{{ route('partner.create') }}" class="btn btn-info float-right" >+ เพิ่มคู่ค้า</a>
        </div>
    </div>

    <!-- Default box -->
    <div class="card">
        {{-- <div class="card-header">
            <h3 class="card-title d-inline-block">ลูกค้า</h3>
            <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">เพิ่มลูกค้า</button>
        </div> --}}

        <!-- /.card-header -->
        <div class="card-body">

            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>รหัส</th>
                        <th>ชื่อ</th>
                        <th>อีเมล</th>
                        <th>เบอร์มือถือ</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($partners as $key => $partner)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $partner->partner_code }}</td>
                        <td>{{ $partner->name }}</td>
                        <td>{{ $partner->email }}</td>
                        <td>{{ $partner->tel }}</td>
                        <td>
                            <a href="{{ route('partner.edit', $partner->id) }}" class="btn btn-sm btn-warning">แก้ไข</a>
                            {!! Form::open(['route' => ['partner.delete', $partner->id], 'method' => 'delete', 'class' => 'btn-del']) !!}
                            {!! Form::submit('ลบ', ['class' => 'btn btn-sm btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

@stop
