@extends('layouts.master')
@section('title', 'Buy Record')
@section('content')

<div class="row page-titles">
    <div class="col-md-5 align-self-center mb-2">
        <h2 class="text-themecolor">รายการซื้อ</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
            <li class="breadcrumb-item active">รายการซื้อ</li>
        </ol>
    </div>
    <div class="col-sm-7 align-self-center">
        <a href="{{ route('buy-record.create') }}" class="btn btn-info float-right">+ สร้างรายการซื้อ</a>
    </div>
</div>

<!-- Default box -->
<div class="card">
    <!-- /.card-header -->
    <div class="card-body">

        <table class="table" id="buy_records">
            <thead>
                <tr>
                    <th>#</th>
                    <th>รายการ</th>
                    <th>วันทำรายการ</th>
                    <th>คู่ค้า</th>
                    <th>ยอดรวม</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($buy_records as $key => $buy_record)
                <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $buy_record->buy_record_code }}</td>
                    <td>{{ $buy_record->date }}</td>
                    <td>{{ $buy_record->partner->name }}</td>
                    <td>{{ $buy_record->result_price }}</td>
                    <td>
                        <a href="{{ route('buy-record.show', $buy_record->id) }}"
                            class="btn btn-sm btn-info">ดูข้อมูล</a>
                    </td>
                </tr>
                @endforeach
            </tbody>



            <div class="card-body">
                <form action="{{ route('Buyimport') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <a class="btn btn-warning" href="{{ route('Buyexport') }}">Export Rec Data</a>
                </form>
                    <div class="input-group input-daterange" style=" width:500px; float:right; ">
                        <span class="input-group-text">Date</span>
                        <input style=" text-align: center;" name="from_date" id="from_date" type="text"
                            placeholder="Start date" aria-label="First name" class="form-control start-date">
                        <input style=" text-align: center;" name="to_date" id="to_date" type="text"
                            placeholder="End date" aria-label="Last name" class="form-control end-date">
                        <button type="button" class="btn btn-outline-secondary docs-datepicker-trigger" name="filter"
                            id="filter">
                            Filter
                        </button>
                        <button type="button" name="refresh" id="refresh"
                            class="btn btn-outline-secondary docs-datepicker-trigger">Refresh</button>
                    </div>
            </div>
        </table>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
@stop
