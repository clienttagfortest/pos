<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Customer;
use App\SellRecord;
use App\SellRecordProduct;
use App\Stock;
use Auth;

class SellRecordController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'login', 'CheckSellRecord']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $auth_user = Auth::user();

        $sell_records = SellRecord::where('company_id', $auth_user->company_id)->get();
        return view('sell-record.index', compact('sell_records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $auth_user = Auth::user();

        $sell_code = $this->generateSellRecord();
        $products = Product::where('company_id', $auth_user->company_id)->get();
        $customers = Customer::where('company_id', $auth_user->company_id)->get();
        return view('sell-record.create', compact('sell_code', 'products', 'customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $auth_user = Auth::user();

        $input = $request->input();
        $result_price = 0;


        foreach ($input['product_code'] as $key => $value) {
            $sell_record_product['sell_record_code'] = $input['sell_record_code'];
            $sell_record_product['product_code'] = $input['product_code'][$key];
            $sell_record_product['name'] = $input['name'][$key];
            $sell_record_product['amount'] = $input['amount'][$key];
            $sell_record_product['unit_price'] = $input['unit_price'][$key];
            $sell_record_product['discount_unit_price'] = $input['discount_unit_price'][$key];
            $sell_record_product['total_price'] = $input['total_price'][$key];
            $sell_record_product['company_id'] = $auth_user->company_id;
            SellRecordProduct::create($sell_record_product);

            $result_price = $result_price + $input['total_price'][$key];

            $stock['product_code'] = $input['product_code'][$key];
            $stock['purchase_price'] = $input['unit_price'][$key];
            $stock['amount'] = $input['amount'][$key];
            $stock['company_id'] = $auth_user->company_id;
            Stock::create($stock);


            $product = Product::where('product_code', $input['product_code'][$key])->where('company_id', $auth_user->company_id)->firstOrFail();
            $product->amount = $product->amount - $input['amount'][$key];
            $product->save();
        }

        $sell_record['sell_record_code'] = $input['sell_record_code'];
        $sell_record['date'] = $input['date'];
        $sell_record['tax_type'] = 'not';
        $sell_record['customer_id'] = $input['customer']['customer_id'];
        $sell_record['result_price'] = $result_price;
        $sell_record['company_id'] = $auth_user->company_id;
        SellRecord::create($sell_record);

        return redirect()->route('sell-record.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $auth_user = Auth::user();
        $sell_record = SellRecord::where('company_id', $auth_user->company_id)->where('id', $id)->firstOrFail();
        return view('sell-record.show', compact('sell_record'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generateSellRecord()
    {
        $user_auth = Auth::user();
        $company_code = $user_auth->company->code;
        $sell_code = $company_code.'S';
        $alphabet = '0123456789';
        for ($i=0; $i < 8; $i++) { 
            $rand = rand(0, strlen($alphabet)-1);
            $sell_code = $sell_code.$alphabet[$rand];
        }
        return $sell_code;
    }
}
