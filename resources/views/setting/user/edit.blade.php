@extends('layouts.master')
@section('title', 'User')
@section('content')


    <div class="row page-titles">
        <div class="col-md-5 align-self-center mb-2">
            <h2 class="text-themecolor">แก้ไขผู้ใช้งาน</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
                <li class="breadcrumb-item"><a href="{{ route('setting.user.index') }}">รายชื่อผู้ใช้งาน</a></li>
                <li class="breadcrumb-item active">แก้ไขผู้ใช้งาน</li>
            </ol>
        </div>
    </div>

    <div class="card card-form">
        {!! Form::model($user, ['route' => ['setting.user.update', $user->id], 'method' => 'patch']) !!}

        <div class="form-group">
            {!! Form::label('name', 'ชื่อ') !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('status', 'สถานะ') !!}
            {!! Form::select('status', [true => 'ใช้งาน', false => 'ไม่ใช่งาน'], null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('role_id', 'สิทธิ์การใช้งาน') !!}
            {!! Form::select('role_id', $roles, null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
        </div>

        {!! Form::close() !!}
    </div>


@stop
