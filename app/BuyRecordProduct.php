<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuyRecordProduct extends Model
{
    protected $connection = 'mysql';
    protected $table = 'buy_record_products';
    protected $fillable = ['buy_record_code', 'product_code', 'name', 'amount', 'unit_price', 'discount_unit_price', 'total_price', 'company_id'];

    function product()
    {
    	return $this->belongsTo('App\Product', 'product_code', 'product_code');
    }
}
