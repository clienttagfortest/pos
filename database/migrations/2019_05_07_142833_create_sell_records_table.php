<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('sell_records');
        Schema::create('sell_records', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sell_record_code');
            $table->date('date');
            $table->string('tax_type')->nullable();
            $table->string('customer_id');
            $table->string('result_price');
            $table->string('company_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sell_records');
    }
}
