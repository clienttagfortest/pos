<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header">
            <a class="navbar-brand" href="../../general-dashboard/clients/index.php">
                <!-- Logo icon -->
                <b>
                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                    <!-- Dark Logo icon -->
                    <img src="../../../../assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
                    <!-- Light Logo icon -->
                    <img src="../../../../assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />
                </b>
                <!--End Logo icon -->
                <!-- Logo text -->
                <span>
                    <!-- dark Logo text -->
                    <img src="../../../../assets/images/logo-text.png" alt="homepage" class="dark-logo" />
                    <!-- Light Logo text -->
                    <img src="../../../../assets/images/logo-light-text.png" class="light-logo" alt="homepage" />
                </span>
            </a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up waves-effect waves-dark" href="javascript:void(0)"><i class="sl-icon-menu"></i></a> </li>
                <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down waves-effect waves-dark" href="javascript:void(0)"><i class="sl-icon-menu"></i></a> </li>
            </ul>
            <!-- ============================================================== -->
            <!-- User profile -->
            <!-- ============================================================== -->
            <ul class="navbar-nav my-lg-0">

                <!-- ============================================================== -->
                <!-- hide sidebar -->
                <!-- ============================================================== -->
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
                </li>

                <!-- ============================================================== -->
                <!-- Profile -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown u-pro">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../../../../assets/images/users/1.jpg" alt="user" class="" /> <span class="hidden-md-down">Phisit Liammukda &nbsp;<i class="fa fa-angle-down"></i></span> </a>
                    <div class="dropdown-menu dropdown-menu-right animated flipInY">
                        <ul class="dropdown-user">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img"><img src="../../../../assets/images/users/1.jpg" alt="user"></div>
                                    <div class="u-text">
                                        <h4>Phisit Liammukda</h4>
                                        <p class="text-muted">phisit.liammukda@gmail.com</p><a href="../../../pages/general/profile" class="btn btn-rounded btn-danger btn-sm">ดูโปรไฟล์</a></div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="../../../pages/general/profile"><i class="ti-user"></i>&nbsp; จัดการโปรไฟล์ธุรกิจ</a></li>
                            <!-- <li><a href="#"><i class="ti-wallet"></i>&nbsp; My Balance</a></li> -->
                            <!-- <li><a href="#"><i class="ti-email"></i>&nbsp; Inbox</a></li> -->
                            <!-- <li role="separator" class="divider"></li> -->
                            <!-- <li><a href="#"><i class="ti-settings"></i>&nbsp; Account Setting</a></li> -->
                            <li role="separator" class="divider"></li>
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-power-off"></i> Logout</a>
                            </li>

                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>

<!-- /.navbar -->
<aside class="main-sidebar sidebar-light-dark elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <span class="brand-text font-weight-light text-center d-block">Client </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <ul class="sidebar-menu">

                    <li><a href="{{ route('dashboard') }}"><i class="fas fa-chart-bar nav-icon"></i> <span>หน้าหลัก</span></a></li>
                    {{-- <li><a href="#"><i class="fas fa-box-open nav-icon"></i> <span>My Devices</span></a></li>
                    <li><a href="#"><i class="fas fa-map-marked-alt nav-icon"></i> <span>Sites</span></a></li> --}}
                    @if(Auth::user()->role_id != null)
                        @if(Auth::user()->role->sell != 'not' && Auth::user()->role->buy != 'not' && Auth::user()->role->brand != 'not'  && Auth::user()->role->product != 'not' && Auth::user()->role->customer != 'not' && Auth::user()->role->finance != 'not' && Auth::user()->role->setting != 'not')
                            <li class="sidebar-header">POS</li>

                            @if(Auth::user()->role->sell != 'not')
                            <li class="nav-item">
                                <a href="#">
                                    <i class="fas fa-cash-register nav-icon"></i>
                                    <span>รายการขาย</span>
                                    <i class="fa fa-angle-left float-right"></i>
                                </a>
                                <ul class="sidebar-submenu">
                                    <li><a href="{{ route('sell-record.create') }}">สร้างรายการขาย</a></li>
                                    <li><a href="{{ route('sell-record.index') }}">ดูรายการขาย</a></li>
                                </ul>
                            </li>
                            @endif

                            @if(Auth::user()->role->buy != 'not')
                            <li class="nav-item">
                                <a href="#">
                                    <i class="fas fa-cart-plus nav-icon"></i>
                                    <span>รายการซื้อ</span>
                                    <i class="fa fa-angle-left float-right"></i>
                                </a>
                                <ul class="sidebar-submenu">
                                    <li><a href="{{ route('buy-record.create') }}">สร้างรายการซื้อ</a></li>
                                    <li><a href="{{ route('buy-record.index') }}">ดูรายการซื้อ</a></li>
                                </ul>
                            </li>
                            @endif

                            @if(Auth::user()->role->brand != 'not'  && Auth::user()->role->product != 'not')
                            <li class="nav-item">
                                <a href="#">
                                    <i class="fas fa-barcode nav-icon"></i>
                                    <span>สินค้า</span>
                                    <i class="fa fa-angle-left float-right"></i>
                                </a>
                                <ul class="sidebar-submenu">
                                    @if(Auth::user()->role->brand != 'not')
                                    <li><a href="{{ route('brand.index') }}">แบรนด์</a></li>
                                    @endif

                                    @if(Auth::user()->role->product != 'not')
                                    <li><a href="{{ route('product.index') }}">สินค้า</a></li>
                                    @endif
                                    <li><a href="{{ route('category.index') }}">หมวดหมู่</a></li>
                                    {{-- <li><a href="#">คลังสินค้า</a></li> --}}
                                </ul>
                            </li>
                            @endif

                            @if(Auth::user()->role->customer != 'not')
                            <li class="nav-item">
                                <a href="#">
                                    <i class="fas fa-user nav-icon"></i>
                                    <span>ลูกค้า/คู่ค้า</span>
                                    <i class="fa fa-angle-left float-right"></i>
                                </a>
                                <ul class="sidebar-submenu">
                                    <li><a href="{{ route('customer.index') }}">รายชื่อลูกค้า</a></li>
                                    <li><a href="{{ route('partner.index') }}">รายชื่อคู่ค้า</a></li>
                                    {{-- <li><a href="../pos-customer/customer.php">รายชื่อลูกค้า</a></li> --}}
                                </ul>
                            </li>
                            @endif

                            @if(Auth::user()->role->finance != 'not')
                            {{-- <li class="nav-item">
                                <a href="#">
                                    <i class="fas fa-money-bill-alt nav-icon"></i>
                                    <span>การเงิน</span>
                                    <i class="fa fa-angle-left float-right"></i>
                                </a>
                                <ul class="sidebar-submenu">
                                    @if(Auth::user()->role->finance == 'income')
                                    <li><a href="{{ route('finance.income.index') }}">รายรับ</a></li>
                                    @endif

                                    @if(Auth::user()->role->finance == 'cost')
                                    <li><a href="{{ route('finance.cost.index') }}">รายจ่าย</a></li>
                                    @endif
                                </ul>
                            </li> --}}
                            @endif

                            @if(Auth::user()->role->setting != 'not')
                            <li class="nav-item">
                                <a href="#">
                                    <i class="fas fa-cog nav-icon"></i>
                                    <span>ตั้งค่า</span>
                                    <i class="fa fa-angle-left float-right"></i>
                                </a>
                                <ul class="sidebar-submenu">
                                    <li><a href="{{ route('setting.business.edit') }}">ข้อมูลธุรกิจ</a></li>
                                    <li><a href="{{ route('setting.user.index') }}">ผู้ใช้งาน</a></li>
                                    <li><a href="{{ route('setting.role.index') }}">สิทธิ์การใช้งาน</a></li>
                                    {{-- <li><a href="#">การแจ้งเตือน</a></li> --}}
                                </ul>
                            </li>
                            @endif
                        @endif
                    @else

                        <li class="sidebar-header">POS</li>


                        <li class="nav-item">
                            <a href="#">
                                <i class="fas fa-cash-register nav-icon"></i>
                                <span>รายการขาย</span>
                                <i class="fa fa-angle-left float-right"></i>
                            </a>
                            <ul class="sidebar-submenu">
                                <li><a href="{{ route('sell-record.create') }}">สร้างรายการขาย</a></li>
                                <li><a href="{{ route('sell-record.index') }}">ดูรายการขาย</a></li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="#">
                                <i class="fas fa-cart-plus nav-icon"></i>
                                <span>รายการซื้อ</span>
                                <i class="fa fa-angle-left float-right"></i>
                            </a>
                            <ul class="sidebar-submenu">
                                <li><a href="{{ route('buy-record.create') }}">สร้างรายการซื้อ</a></li>
                                <li><a href="{{ route('buy-record.index') }}">ดูรายการซื้อ</a></li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="#">
                                <i class="fas fa-barcode nav-icon"></i>
                                <span>สินค้า</span>
                                <i class="fa fa-angle-left float-right"></i>
                            </a>
                            <ul class="sidebar-submenu">

                                <li><a href="{{ route('brand.index') }}">แบรนด์</a></li>



                                <li><a href="{{ route('product.index') }}">สินค้า</a></li>
                                <li><a href="{{ route('category.index') }}">หมวดหมู่</a></li>

                                {{-- <li><a href="#">คลังสินค้า</a></li> --}}
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="#">
                                <i class="fas fa-user nav-icon"></i>
                                <span>ลูกค้า/คู่ค้า</span>
                                <i class="fa fa-angle-left float-right"></i>
                            </a>
                            <ul class="sidebar-submenu">
                                <li><a href="{{ route('customer.index') }}">รายชื่อลูกค้า</a></li>
                                <li><a href="{{ route('partner.index') }}">รายชื่อคู่ค้า</a></li>
                                {{-- <li><a href="../pos-customer/customer.php">รายชื่อลูกค้า</a></li> --}}
                            </ul>
                        </li>

                        {{-- <li class="nav-item">
                            <a href="#">
                                <i class="fas fa-money-bill-alt nav-icon"></i>
                                <span>การเงิน</span>
                                <i class="fa fa-angle-left float-right"></i>
                            </a>
                            <ul class="sidebar-submenu">
                                <li><a href="{{ route('finance.income.index') }}">รายรับ</a></li>
                                <li><a href="{{ route('finance.cost.index') }}">รายจ่าย</a></li>
                            </ul>
                        </li> --}}

                        <li class="nav-item">
                            <a href="#">
                                <i class="fas fa-cog nav-icon"></i>
                                <span>ตั้งค่า</span>
                                <i class="fa fa-angle-left float-right"></i>
                            </a>
                            <ul class="sidebar-submenu">
                                <li><a href="{{ route('setting.business.edit') }}">ข้อมูลธุรกิจ</a></li>
                                <li><a href="{{ route('setting.user.index') }}">ผู้ใช้งาน</a></li>
                                <li><a href="{{ route('setting.role.index') }}">สิทธิ์การใช้งาน</a></li>
                                {{-- <li><a href="#">การแจ้งเตือน</a></li> --}}
                            </ul>
                        </li>

                    @endif


                    {{-- <li class="sidebar-header"> </li> --}}

                    <li>
                        {{-- <a href="#" class="nav-link"><i class="fas fa-sign-out-alt nav-icon"></i> <span>ออกจากระบบ</span></a> --}}

                        <a class="nav-link" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt nav-icon"></i> <span>ออกจากระบบ</span>
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>

                </ul>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
