<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Partner;
use App\BuyRecord;
use App\BuyRecordProduct;
use App\Stock;
use Auth;

class BuyRecordController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'login', 'CheckBuyRecord']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $auth_user = Auth::user();

        $buy_records = BuyRecord::where('company_id', $auth_user->company_id)->get();
        return view('buy-record.index', compact('buy_records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $auth_user = Auth::user();

        $buy_code = $this->generateBuyRecord();
        $products = Product::where('company_id', $auth_user->company_id)->get();
        $partners = Partner::where('company_id', $auth_user->company_id)->get();
        return view('buy-record.create', compact('buy_code', 'products', 'partners'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $auth_user = Auth::user();

        $input = $request->input();
        $result_price = 0;

        // dd($input);




        foreach ($input['product_code'] as $key => $value) {
            $buy_record_product['buy_record_code'] = $input['buy_record_code'];
            $buy_record_product['product_code'] = $input['product_code'][$key];
            $buy_record_product['name'] = $input['name'][$key];
            $buy_record_product['amount'] = $input['amount'][$key];
            $buy_record_product['unit_price'] = $input['unit_price'][$key];
            $buy_record_product['discount_unit_price'] = $input['discount_unit_price'][$key];
            $buy_record_product['total_price'] = $input['total_price'][$key];
            $buy_record_product['company_id'] = $auth_user->company_id;
            BuyRecordProduct::create($buy_record_product);

            $result_price = $result_price + $input['total_price'][$key];

            $stock['product_code'] = $input['product_code'][$key];
            $stock['purchase_price'] = $input['unit_price'][$key];
            $stock['amount'] = $input['amount'][$key];
            $stock['company_id'] = $auth_user->company_id;
            Stock::create($stock);


            $product = Product::where('product_code', $input['product_code'][$key])->where('company_id', $auth_user->company_id)->firstOrFail();
            $product->amount = $product->amount + $input['amount'][$key];
            $product->save();
        }

        $buy_record['buy_record_code'] = $input['buy_record_code'];
        $buy_record['date'] = $input['date'];
        $buy_record['tax_type'] = 'not';
        $buy_record['partner_id'] = $input['partner']['partner_id'];
        $buy_record['result_price'] = $result_price;
        $buy_record['company_id'] = $auth_user->company_id;
        BuyRecord::create($buy_record);

        return redirect()->route('buy-record.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $auth_user = Auth::user();
        $buy_record = BuyRecord::where('company_id', $auth_user->company_id)->where('id', $id)->firstOrFail();
        return view('buy-record.show', compact('buy_record'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generateBuyRecord()
    {
        $auth_user = Auth::user();
        $company_code = $auth_user->company->code;
        $buy_code = $company_code.'B';
        $alphabet = '0123456789';
        for ($i=0; $i < 8; $i++) {
            $rand = rand(0, strlen($alphabet)-1);
            $buy_code = $buy_code.$alphabet[$rand];
        }
        return $buy_code;
    }
}
