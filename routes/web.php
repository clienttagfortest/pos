<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
 * Export Sell Record Excel
 */
Route::get('generate-pdf','PDFController@generatePDF');

/*
 * Export Sell Record Excel
 */
Route::get('Sellexport', 'MyControllerSell@export')->name('Sellexport');
Route::get('SellimportExportView', 'MyControllerSell@SellimportExportView');
Route::post('Sellimport', 'MyControllerSell@import')->name('Sellimport');
/*
 * Export Buy Record Excel
 */
Route::get('Buyexport', 'MyControllerBuy@export')->name('Buyexport');
Route::get('BuyimportExportView', 'MyControllerBuy@BuyimportExportView');
Route::post('Buyimport', 'MyControllerBuy@import')->name('Buyimport');
/*
 * DataRangeController
 */
Route::resource('daterange','DataRangeController');

Route::get('/', 'HomeController@dashboard')->name('dashboard');


Route::get('/sell-record', 'SellRecordController@index')->name('sell-record.index');
Route::get('/sell-record/create', 'SellRecordController@create')->name('sell-record.create');
Route::post('/sell-record', 'SellRecordController@store')->name('sell-record.store');
Route::get('/sell-record/{id}', 'SellRecordController@show')->name('sell-record.show');

Route::get('/buy-record', 'BuyRecordController@index')->name('buy-record.index');
Route::get('/buy-record/create', 'BuyRecordController@create')->name('buy-record.create');
Route::post('/buy-record', 'BuyRecordController@store')->name('buy-record.store');
Route::get('/buy-record/{id}', 'BuyRecordController@show')->name('buy-record.show');
Route::get('/buy-record/{id}/edit', 'BuyRecordController@edit')->name('buy-record.edit');
Route::patch('/buy-record/{id}', 'BuyRecordController@update')->name('buy-record.update');
Route::delete('/buy-record/{id}', 'BuyRecordController@destroy')->name('buy-record.delete');

Route::get('/brand', 'BrandController@index')->name('brand.index');
Route::get('/brand/create', 'BrandController@create')->name('brand.create');
Route::post('/brand', 'BrandController@store')->name('brand.store');
Route::get('/brand/{id}/edit', 'BrandController@edit')->name('brand.edit');
Route::patch('/brand/{id}', 'BrandController@update')->name('brand.update');
Route::delete('/brand/{id}', 'BrandController@destroy')->name('brand.delete');

Route::get('/category', 'CategoryController@index')->name('category.index');
Route::get('/category/create', 'CategoryController@create')->name('category.create');
Route::post('/category', 'CategoryController@store')->name('category.store');
Route::get('/category/{id}/edit', 'CategoryController@edit')->name('category.edit');
Route::patch('/category/{id}', 'CategoryController@update')->name('category.update');
Route::delete('/category/{id}', 'CategoryController@destroy')->name('category.delete');

Route::get('/product', 'ProductController@index')->name('product.index');
Route::get('/product/create', 'ProductController@create')->name('product.create');
Route::post('/product', 'ProductController@store')->name('product.store');
Route::get('/product/{id}/edit', 'ProductController@edit')->name('product.edit');
Route::patch('/product/{id}', 'ProductController@update')->name('product.update');
Route::delete('/product/{id}', 'ProductController@destroy')->name('product.delete');

Route::get('/customer', 'CustomerController@index')->name('customer.index');
Route::get('/customer/create', 'CustomerController@create')->name('customer.create');
Route::post('/customer', 'CustomerController@store')->name('customer.store');
Route::get('/customer/{id}/edit', 'CustomerController@edit')->name('customer.edit');
Route::patch('/customer/{id}', 'CustomerController@update')->name('customer.update');
Route::delete('/customer/{id}', 'CustomerController@destroy')->name('customer.delete');

Route::get('/partner', 'PartnerController@index')->name('partner.index');
Route::get('/partner/create', 'PartnerController@create')->name('partner.create');
Route::post('/partner', 'PartnerController@store')->name('partner.store');
Route::get('/partner/{id}/edit', 'PartnerController@edit')->name('partner.edit');
Route::patch('/partner/{id}', 'PartnerController@update')->name('partner.update');
Route::delete('/partner/{id}', 'PartnerController@destroy')->name('partner.delete');

Route::get('/finance/income', 'Finance\IncomeController@index')->name('finance.income.index');
Route::get('/finance/income/create', 'Finance\IncomeController@create')->name('finance.income.create');

Route::get('/finance/cost', 'Finance\CostController@index')->name('finance.cost.index');



Route::get('/setting/bussiness/create', 'Setting\BusinessController@create')->name('setting.business.create');
Route::post('/setting/bussiness', 'Setting\BusinessController@store')->name('setting.business.store');
Route::get('/setting/bussiness', 'Setting\BusinessController@edit')->name('setting.business.edit');
Route::patch('/setting/bussiness', 'Setting\BusinessController@update')->name('setting.business.update');



Route::get('/setting/user', 'Setting\UserController@index')->name('setting.user.index');
Route::get('/setting/user/create', 'Setting\UserController@create')->name('setting.user.create');
Route::post('/setting/user', 'Setting\UserController@store')->name('setting.user.store');
Route::get('/setting/user/{id}/edit', 'Setting\UserController@edit')->name('setting.user.edit');
Route::patch('/setting/user/{id}', 'Setting\UserController@update')->name('setting.user.update');



Route::get('/setting/role', 'Setting\RoleController@index')->name('setting.role.index');
Route::get('/setting/role/create', 'Setting\RoleController@create')->name('setting.role.create');
Route::post('/setting/role', 'Setting\RoleController@store')->name('setting.role.store');
Route::get('/setting/role/{id}/edit', 'Setting\RoleController@edit')->name('setting.role.edit');
Route::patch('/setting/role/{id}', 'Setting\RoleController@update')->name('setting.role.update');
Route::delete('/setting/role/{id}', 'Setting\RoleController@destroy')->name('setting.role.destroy');


Auth::routes();



Route::get('/test', 'CustomerController@generateCustomerCode');
