<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

use App\Role;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'login', 'CheckSetting']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $roles = Role::where('company_id', $user->company_id)->get();
        return view('setting.role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('setting.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $input = $request->input();
        $input['company_id'] = $user->company_id;
        Role::create($input);
        return redirect()->route('setting.role.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $user = Auth::user();

        $role = Role::where('company_id', $user->company_id)->where('id', $id)->firstOrFail();
        return view('setting.role.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        $input = $request->input();
        $role = Role::where('company_id', $user->company_id)->where('id', $id)->firstOrFail();
        $role->update($input);
        return redirect()->route('setting.role.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        
        $role = Role::where('company_id', $user->company_id)->where('id', $id)->firstOrFail();
        $role->delete();
        return redirect()->route('setting.role.index');
    }
}
