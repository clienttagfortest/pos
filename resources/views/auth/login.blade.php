<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>เข้าสู่ระบบ</title>

    <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <script src="{{ asset('assets/jquery/dist/jquery.min.js') }}"></script>
</head>
<body class="auth-page">
    <div class="auth-box">
        <h1>เข้าสู่ระบบ</h1>
        <hr>
        {!! Form::open(['route' => 'login', 'method' => 'post']) !!}

            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Username">
            </div>

            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password">
            </div>

            <div class="form-group align-center">
                <button type="submit" class="btn btn-primary">เข้าสู่ระบบ</button>
            </div>

        {!! Form::close() !!}

        <div class="auth-bottom-box align-right">
            <a href="{{ route('register') }}">สมัครสมาชิก</a>
        </div>
    </div>


</body>
</html>