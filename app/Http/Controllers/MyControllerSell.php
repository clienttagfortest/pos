<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\SellRecordExport;
use App\Imports\SellRecordImport;
use Maatwebsite\Excel\Facades\Excel;

class MyControllerSell extends Controller
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function importExportView()
    {
       return view('import');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export()
    {
        return Excel::download(new SellRecordExport, 'SellRecord.xlsx');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function import()
    {
        Excel::import(new SellRecordImport,request()->file('file'));

        return back();
    }
}
