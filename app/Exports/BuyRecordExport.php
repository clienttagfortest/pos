<?php

namespace App\Exports;

use App\BuyRecordProduct;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class BuyRecordExport implements FromCollection, WithHeadings, ShouldAutoSize,  WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return BuyRecordProduct::all();
    }
    public function headings(): array
    {
        // return SellRecordProduct::all();
        return [
            '#',
            'Buy_code',
            'Product_code',
            'Name',
            'Amount',
            'Unit Price',
            'Discount',
            'Total Price',
            'Company_ID',
            'Date'
        ];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(18);
            },
        ];
    }
}
