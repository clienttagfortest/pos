@extends('layouts.master')
@section('title', 'Buy Record')
@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center mb-2">
            <h2 class="text-themecolor">รายการซื้อ</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
                <li class="breadcrumb-item active">รายการซื้อ</li>
            </ol>
        </div>
        {{-- <div class="col-sm-7 align-self-center">
            <a href="{{ route('buy-record.create') }}" class="btn btn-info float-right" >+ สร้างรายการซื้อ</a>
        </div> --}}
    </div>

    <!-- Default box -->
    <div class="card">
        <!-- /.card-header -->
        <div class="card-body">

            <div class="nav-btn">
                <a href="{{action('PDFController@generatePDF')}}" class="btn btn-sm btn-primary"><i class="fas fa-print"></i> พิมพ์เอกสาร</a>
            </div>

            <hr>

            <div class="row">
                <div class="col-sm-6">

                    <div class="row">
                        <div class="col-sm-6">รายการ</div>
                        <div class="col-sm-6">{{ $buy_record->buy_record_code }}</div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">ประเภทรายการ</div>
                        <div class="col-sm-6">ซื้อสินค้าเข้า</div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">วันทำรายการ</div>
                        <div class="col-sm-6">{{ $buy_record->date }}</div>
                    </div>

                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6"></div>
                    </div>
                </div>
            </div>

            <hr>

            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>รูปภาพ</th>
                        <th>รหัส</th>
                        <th>ชื่อสินค้า</th>
                        <th>จำนวน</th>
                        <th>มูลค่าต่อหน่วย</th>
                        <th>ส่วนลดต่อหน่วย</th>
                        <th>รวม</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($buy_record->buy_record_products as $key => $buy_record_product)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td><img src="{{ $buy_record_product->product->brand->image }}" alt="" width="50" height="50"></td>
                        <td>{{ $buy_record_product->product_code }}</td>
                        <td>{{ $buy_record_product->name }}</td>
                        <td>{{ $buy_record_product->amount }}</td>
                        <td>{{ $buy_record_product->unit_price }}</td>
                        <td>{{ $buy_record_product->discount_unit_price }}</td>
                        <td>{{ $buy_record_product->total_price }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <hr>
            <div class="row">
                <div class="col-sm-6"></div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-6">รวม</div>
                        <div class="col-sm-6">{{ $buy_record->result_price }}</div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

@stop
