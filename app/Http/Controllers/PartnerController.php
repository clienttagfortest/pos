<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Partner;


class PartnerController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'login', 'CheckCustomer']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_auth = Auth::user();
        
        $partners = Partner::where('company_id', $user_auth->company_id)->get();
        return view('partner.index', compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('partner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_auth = Auth::user();

        $input = $request->input();
        $partner_code = $this->generatePartnerCode();
        $input['partner_code'] = $partner_code;
        $input['company_id'] = $user_auth->company_id;
        Partner::create($input);
        return redirect()->route('partner.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_auth = Auth::user();

        $partner = Partner::where('company_id', $user_auth->company_id)->where('id', $id)->firstOrFail();
        return view('partner.edit', compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_auth = Auth::user();

        $input = $request->input();
        $partner = Partner::where('company_id', $user_auth->company_id)->where('id', $id)->firstOrFail();
        $partner->update($input);
        return redirect()->route('partner.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_auth = Auth::user();

        $partner = Partner::where('company_id', $user_auth->company_id)->where('id', $id)->firstOrFail();
        $partner->delete();
        return redirect()->route('partner.index');
    }


    public function generatePartnerCode()
    {
        $user_auth = Auth::user();
        $company_code = $user_auth->company->code;
        $partner_code = $company_code.'C';
        $alphabet = '0123456789';
        for ($i=0; $i < 8; $i++) { 
            $rand = rand(0, strlen($alphabet)-1);
            $partner_code = $partner_code.$alphabet[$rand];
        }
        return $partner_code;
    }
}
