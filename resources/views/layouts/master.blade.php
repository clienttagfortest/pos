<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>@yield('title')</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/dist/css/bootstrap.min.css') }}">

    {{-- <link rel="stylesheet" href="{{ asset('assets/plugins/dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/dist/sidebar-menu.css') }}"> --}}

    <!-- chartist CSS -->
    <link rel="stylesheet" href="{{ asset('css/morris.css') }}">
    <!--c3 CSS -->
    <link rel="stylesheet" href="{{ asset('css/c3.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!-- Dashboard Page CSS -->
    <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <!-- Datatables CSS -->
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

    <!-- Datatpicker CSS -->
    <link rel="stylesheet" href="https://fengyuanchen.github.io/datepicker/css/datepicker.css">


</head>
<body class="fix-header fix-sidebar card-no-border">

        <!-- Navbar & Main Sidebar Container -->
        @include('../layouts/sidebar')

        <!-- ============================================================== -->
        <!-- Main wrapper -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">


                    @yield('content')


                </div>
                <!-- ============================================================== -->
                <!-- End Container fluid  -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Page wrapper  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->


        <!-- footer -->
        @include('../layouts/footer')


    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{ asset('assets/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap popper Core JavaScript -->
    <script src="{{ asset('assets/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('assets/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    {{-- <script src="{{ asset('assets/plugins/dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/dist/sidebar-menu.js') }}"></script> --}}

    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('js/perfect-scrollbar.jquery.min.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ asset('js/waves.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('js/sidebarmenu.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('js/custom.min.js') }}"></script>
    <!--morris JavaScript -->
    <script src="{{ asset('js/raphael-min.js') }}"></script>
    <script src="{{ asset('js/morris.min.js') }}"></script>
    <!--c3 JavaScript -->
    <script src="{{ asset('js/d3.min.js') }}"></script>
    <script src="{{ asset('js/c3.min.js') }}"></script>
    <!-- Chart JS -->
    <script src="{{ asset('js/dashboard.js') }}"></script>
    {{-- Datatables JS --}}
    <script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
     {{-- Datatapicker JS --}}
     <script src="https://fengyuanchen.github.io/datepicker/js/datepicker.js"></script>


    <script>
        $(function () {
            $('#dataTable').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
            });
        });

        $.sidebarMenu($('.sidebar-menu'))


    </script>



     <script>
        $(document).ready(function(){
        var $startDate = $('.start-date');
        var $endDate = $('.end-date');

        $startDate.datepicker({
        autoHide: true,
        format:'yyyy-mm-dd'
    });
        $endDate.datepicker({
        autoHide: true,
        startDate: $startDate.datepicker('getDate'),
        format:'yyyy-mm-dd'
    });

        $startDate.on('change', function () {
        $endDate.datepicker('setStartDate', $startDate.datepicker('getDate'));
    });
        $().datepicker({
        language: 'en-GB'
    });
        $().on('pick.datepicker', function (e) {
        if (e.date < new Date()) {
        e.preventDefault(); // Prevent to pick the date
         }
    });

        // load_data();

        function load_data(from_date = '', to_date = '')
                {
                    $('#buy_records').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url:'{{ route("daterange.index") }}',
                        data:{from_date:from_date, to_date:to_date}
                    },
                    columns: [
                        {
                            data:'',
                            name:'id'
                        },
                        {
                            data:'buy_record_code',
                            name:'buy_record_code'
                        },
                        {
                            data:'date ',
                            name:'date '
                        },
                        {
                            data:'name',
                            name:'name'
                        },
                        {
                            data:'result_price',
                            name:'result_price'
                        }
                    ]
                });
            }

        $('#filter').click(function(){
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if(from_date != '' &&  to_date != '')
            {
                $('#buy_records').DataTable().destroy();
                load_data(from_date, to_date);
            }
            else
            {
                alert('Both Date is required');
            }
        });

        $('#refresh').click(function(){
            $('#from_date').val('');
            $('#to_date').val('');
            $('#buy_records').DataTable().destroy();
            load_data();
        });
});
    </script>
</body>
</html>

{{--
//--Back-up Script --//
--}}

{{-- <script>
    $(function() {
    var $startDate = $('.start-date');
    var $endDate = $('.end-date');

    $startDate.datepicker({
    autoHide: true,
    format:'yyyy-mm-dd'
});
    $endDate.datepicker({
    autoHide: true,
    startDate: $startDate.datepicker('getDate'),
    format:'yyyy-mm-dd'
});

    $startDate.on('change', function () {
    $endDate.datepicker('setStartDate', $startDate.datepicker('getDate'));
});
    $().datepicker({
    language: 'en-GB'
});
    $().on('pick.datepicker', function (e) {
    if (e.date < new Date()) {
    e.preventDefault(); // Prevent to pick the date
     }
});

    $('#filter').click(function(){
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        if(from_date != '' &&  to_date != '')
        {
            $('#buy_records').DataTable().destroy();
            load_data(from_date, to_date);
        }
        else
        {
            alert('Both Date is required');
        }
    });

    $('#refresh').click(function(){
        $('#from_date').val('');
        $('#to_date').val('');
        $('#buy_records').DataTable().destroy();
        load_data();
    });
});
</script> --}}
