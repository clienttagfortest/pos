<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Company;
use App\User;
use App\Role;
use Auth;

class BusinessController extends Controller
{

      public function __construct()
      {
         $this->middleware(['auth', 'CheckSetting']);
      }

      public function create()
      {
         $user_auth = Auth::user();
         if ($user_auth->company_id != null) {
            return redirect()->route('setting.business.edit');
         }
         return view('setting.business.create');
      }

      public function store(Request $request)
      {
         $user_auth = Auth::user();
         $input = $request->input();
         $company = Company::create($input);

         $user = User::find($user_auth->id);
         $user->company_id = $company->id;
         $user->save();

         // $role['name'] = 'root';
         // $role['sell'] = 'all';
         // $role['buy'] = 'all';
         // $role['product'] = 'all';
         // $role['brand'] = 'all';
         // $role['warehouse'] = 'all';
         // $role['customer'] = 'all';
         // $role['finance'] = 'all';
         // $role['setting'] = 'all';
         // $role['company_id'] = $company->id;
         // Role::create($role);

         return redirect()->route('dashboard');
      }

   	public function edit()
   	{
   		$user = Auth::user();
   		$company = Company::where('id', $user->company_id)->first();
   		return view('setting.business.edit', compact('company'));
   	}

   	public function update(Request $request)
   	{
         $user_auth = Auth::user();
         $input = $request->input();
         $company = Company::where('id', $user_auth->company_id)->firstOrFail();
         $company->update($input);
         return redirect()->route('setting.business.edit');
   	}

}
