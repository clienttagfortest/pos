@extends('layouts.master')
@section('title', 'Business')
@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center mb-2">
            <h2 class="text-themecolor">แก้ไขข้อมูล บริษัท/ร้านค้า</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
                <li class="breadcrumb-item active">แก้ไขข้อมูล บริษัท/ร้านค้า</li>
            </ol>
        </div>
    </div>

    <!-- Default box -->
    <div class="card">

        <div class="card-body">

        {!! Form::model($company ,['route' => 'setting.business.update', 'method' => 'patch']) !!}

        <div class="form-group">
            {!! Form::label('name', 'บริษัท/ร้านค้า') !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('tax_no', 'เลขผู้เสียภาษี') !!}
            {!! Form::number('tax_numbar', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('website', 'Website') !!}
            {!! Form::text('website', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('tel', 'Tel') !!}
            {!! Form::number('tel', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('code', 'รหัส') !!}
            {!! Form::pasword('code', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('email', 'Email') !!}
            {!! Form::email('email', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('address', 'ที่อยู่') !!}
            {{-- <textarea name="address" id="" cols="" rows="3" class="form-control"></textarea> --}}
            {!! Form::textarea('address', null,['class' => 'form-control', 'rows' => '3', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
        </div>

        {!! Form::close() !!}

        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

@stop
