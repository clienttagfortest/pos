<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Category;
use App\Product;
use App\Brand;
use App\Stock;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'login', 'CheckProduct']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_auth = Auth::user();

        $products = Product::where('company_id', $user_auth->company_id)->get();
        return view('product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::pluck('name', 'id');
        $categories = Category::pluck('name', 'id');
        return view('product.create', compact('categories', 'brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_auth = Auth::user();

        $input = $request->input();

        $product['product_code'] = $this->generateProductCode();
        $product['name'] = $input['name'];
        $product['category_id'] = $input['category_id'];
        $product['unit_text'] = $input['unit_text'];
        $product['company_id'] = $user_auth->company_id;
        $product['brand_id'] = $input['brand_id'];
        $product['tag'] = $input['tag'];
        $product['purchase_price'] = $input['purchase_price'];
        $product['sale_price'] = $input['sale_price'];
        $product['amount'] = $input['amount'];
        $product = Product::create($product);
        // dd($product->id);


        $stock['product_id'] = $product->id;
        $stock['product_code'] = $product['product_code'];
        $stock['purchase_price'] = $input['purchase_price'];
        // $stock['sale_price'] = $input['sale_price'];
        $stock['amount'] =  $input['amount'];
        $stock['company_id'] = $user_auth->company_id;
        // dd($stock);
        Stock::create($stock);

        // Product::create()

        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_auth = Auth::user();
        // dd($user_auth);
        $brands = Brand::pluck('name', 'id');
        $categories = Category::pluck('name', 'id');
        $product = Product::where('company_id', $user_auth->company_id)->where('id', $id)->firstOrFail();
        return view('product.edit', compact('product', 'categories', 'brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_auth = Auth::user();

        $input = $request->input();
        $product = Product::where('company_id', $user_auth->company_id)->where('id', $id)->firstOrFail();
        $product->update($input);
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_auth = Auth::user();

        $product = Product::where('company_id', $user_auth->company_id)->where('id', $id)->firstOrFail();
        $product->delete();
        return redirect()->route('product.index');   
    }

    public function generateProductCode()
    {
        $user_auth = Auth::user();
        $company_code = $user_auth->company->code;
        $product_code = $company_code.'P';
        $alphabet = '0123456789';
        for ($i=0; $i < 8; $i++) { 
            $rand = rand(0, strlen($alphabet)-1);
            $product_code = $product_code.$alphabet[$rand];
        }
        return $product_code;
    }
}
