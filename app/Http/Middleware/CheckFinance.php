<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckFinance
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->role_id == null) {
            return $next($request);
        } else {
            $user_role = Auth::user()->role->buy;
            // dd($user_role);
            if ($user_role == 'not') {
                return abort(404);
            } else if($user_role == 'income') {
                return redirect()->route('finance.income.index');
            } else if($user_role == 'cost') {
                return redirect()->route('finance.cost.index');
            } else {
                return $next($request);
            }
        }
    }
}
