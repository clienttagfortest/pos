<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuyRecordProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('buy_record_products');
        Schema::create('buy_record_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('buy_record_code');
            $table->string('product_code');
            $table->string('name');
            $table->string('amount');
            $table->string('unit_price');
            $table->string('discount_unit_price')->nullable();
            $table->string('total_price');
            $table->string('company_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buy_record_products');
    }
}
