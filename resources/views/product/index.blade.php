@extends('layouts.master')
@section('title', 'Product')
@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center mb-2">
            <h2 class="text-themecolor">สินค้า</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
                <li class="breadcrumb-item active">สินค้า</li>
            </ol>
        </div>
        <div class="col-sm-7 align-self-center">
            <a href="{{ route('product.create') }}" class="btn btn-info float-right" >+ เพิ่มสินค้า</a>
        </div>
    </div>

    <!-- Default box -->
    <div class="card">
        {{-- <div class="card-header">
            <h3 class="card-title d-inline-block">ลูกค้า</h3>
            <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">เพิ่มลูกค้า</button>
        </div> --}}

        <!-- /.card-header -->
        <div class="card-body">

            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>รหัส</th>
                        <th>ชื่อ</th>
                        <th>ราคาซื้อ</th>
                        <th>ราคาขาย</th>
                        <th>คงเหลือ</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $key => $product)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $product->product_code }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->purchase_price}}</td>
                        <td>{{ $product->sale_price }}</td>
                        <td>{{ $product->amount }}</td>
                        <td>
                            <div class="dropdown">
                                <a  class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-cog"></i>
                                </a>
                                <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ route('buy-record.create') }}">ซื้อสินค้า</a>
                                <a class="dropdown-item" href="#">ขายสินค้า</a>
                                <hr>
                                <a class="dropdown-item" href="{{ route('product.edit', $product->id) }}">แก้ไข</a>
                                {{-- <a class="dropdown-item" href="#">ลบ</a> --}}
                                {!! Form::open(['route' => ['product.delete', $product->id], 'method' => 'delete']) !!}
                                {!! Form::submit('ลบ', ['class' => 'dropdown-item']) !!}
                                {!! Form::close() !!}
                                </div>
                            </div>
                        </td>
                        {{-- <td>
                            <a href="{{ route('brand.edit', $brand->id) }}" class="btn btn-sm btn-warning">แก้ไข</a>
                            {!! Form::open(['route' => ['brand.delete', $brand->id], 'method' => 'delete', 'class' => 'btn-del']) !!}
                            {!! Form::submit('ลบ', ['class' => 'btn btn-sm btn-danger']) !!}
                            {!! Form::close() !!}
                        </td> --}}
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

@stop
