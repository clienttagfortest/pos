<?php

namespace App\Imports;

use App\BuyRecordProduct;
use App\Company;
use Maatwebsite\Excel\Concerns\ToModel;

class BuyRecordImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new BuyRecordProduct([
            'id' => $row[0],
            'buy_record_code' => $row[1],
            'product_code' => $row[2],
            'name' => $row[3],
            'amount' => $row[4],
            'unit_price' => $row[5],
            'discount_unit_price' => $row[6],
            'total_price' => $row[7],
            'name' => $row[7],
            'date' => $row[8]

        ]);
    }
}
