@extends('layouts.master')
@section('title', 'Product')
@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center mb-2">
            <h2 class="text-themecolor">แก้ไขข้อมูลสินค้า</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
                <li class="breadcrumb-item"><a href="{{ route('product.index') }}">รายการสินค้า</a></li>
                <li class="breadcrumb-item active">แก้ไขข้อมูลสินค้า</li>
            </ol>
        </div>
    </div>

    <!-- Default box -->
    <div class="card">

        <div class="card-body">

        {!! Form::model($product, ['route' => ['product.update', $product->id], 'method' => 'patch']) !!}

        <div class="form-group">
            {!! Form::label('name', 'ชื่อสินค้า') !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('category_id', 'หมวดหมู่') !!}
            {!! Form::select('category_id', $categories, null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('brand_id', 'แบรนด์') !!}
            {!! Form::select('brand_id', $brands, null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('purchase_price', 'ราคาซื้อ') !!}
            {!! Form::number('purchase_price', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('sale_price', 'ราคาขาย') !!}
            {!! Form::number('sale_price', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('unit_text', 'ชื่อหน่วย') !!}
            {!! Form::text('unit_text', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('tag', 'Tag') !!}
            {!! Form::text('tag', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
        </div>

        {!! Form::close() !!}

        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

@stop
