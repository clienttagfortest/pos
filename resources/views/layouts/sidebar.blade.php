<!-- ============================================================== -->
<!-- Topbar header -->
<!-- ============================================================== -->
<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header">
            <a class="navbar-brand" href="../../general-dashboard/clients/index.php">
                <!-- Logo icon -->
                <b>
                    <!-- Dark Logo icon -->
                    <img src="../../../../images/logo-icon.png" alt="homepage" class="dark-logo">
                    <!-- Light Logo icon -->
                    <img src="../../../../images/logo-light-icon.png" alt="homepage" class="light-logo">
                </b>
                <!--End Logo icon -->
                <!-- Logo text -->
                <span>
                    <!-- dark Logo text -->
                    <img src="../../../../images/logo-text.png" alt="homepage" class="dark-logo">
                    <!-- Light Logo text -->
                    <img src="../../../../images/logo-light-text.png" class="light-logo" alt="homepage">
                </span>
            </a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up waves-effect waves-dark" href="javascript:void(0)"><i class="fas fa-bars"></i></a> </li>
                <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down waves-effect waves-dark" href="javascript:void(0)"><i class="fas fa-bars"></i></a> </li>
            </ul>
            <!-- ============================================================== -->
            <!-- User profile -->
            <!-- ============================================================== -->
            <ul class="navbar-nav my-lg-0">
                <!-- ============================================================== -->
                <!-- Comment -->
                <!-- ============================================================== -->
                <!--
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="icon-Bell"></i>
                        <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown">
                        <ul>
                            <li>
                                <div class="drop-title">Notifications</div>
                            </li>
                            <li>
                                <div class="message-center">

                                    <a href="#">
                                        <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>
                                        <div class="mail-contnet">
                                            <h5>Luanch Admin</h5> <span class="mail-desc">Just see the my new admin!</span> <span class="time">9:30 AM</span> </div>
                                    </a>

                                    <a href="#">
                                        <div class="btn btn-success btn-circle"><i class="ti-calendar"></i></div>
                                        <div class="mail-contnet">
                                            <h5>Event today</h5> <span class="mail-desc">Just a reminder that you have event</span> <span class="time">9:10 AM</span> </div>
                                    </a>

                                    <a href="#">
                                        <div class="btn btn-info btn-circle"><i class="ti-settings"></i></div>
                                        <div class="mail-contnet">
                                            <h5>Settings</h5> <span class="mail-desc">You can customize this template as you want</span> <span class="time">9:08 AM</span> </div>
                                    </a>

                                    <a href="#">
                                        <div class="btn btn-primary btn-circle"><i class="ti-user"></i></div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                    </div>
                </li>
                -->
                <!-- ============================================================== -->
                <!-- End Comment -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Messages -->
                <!-- ============================================================== -->
                <!--
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="icon-Mail"></i>
                        <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                    </a>
                    <div class="dropdown-menu mailbox dropdown-menu-right animated bounceInDown" aria-labelledby="2">
                        <ul>
                            <li>
                                <div class="drop-title">You have 4 new messages</div>
                            </li>
                            <li>
                                <div class="message-center">

                                    <a href="#">
                                        <div class="user-img"> <img src="../../../../assets/images/users/1.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                                    </a>

                                    <a href="#">
                                        <div class="user-img"> <img src="../../../../assets/images/users/2.jpg" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Sonu Nigam</h5> <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
                                    </a>

                                    <a href="#">
                                        <div class="user-img"> <img src="../../../../assets/images/users/3.jpg" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Arijit Sinh</h5> <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
                                    </a>

                                    <a href="#">
                                        <div class="user-img"> <img src="../../../../assets/images/users/4.jpg" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <a class="nav-link text-center" href="javascript:void(0);"> <strong>See all e-Mails</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                    </div>
                </li>
                -->
                <!-- ============================================================== -->
                <!-- End Messages -->
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Language -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="flag-icon flag-icon-th"></i></a>
                    <div class="dropdown-menu dropdown-menu-right animated bounceInDown"> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-us"></i> English (US)</a> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-fr"></i> French</a> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-cn"></i> China</a> </div>
                </li>
                <!-- ============================================================== -->
                <!-- Profile -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown u-pro">
                    {{-- <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../../../../assets/images/users/1.jpg" alt="user" class="" /> <span class="hidden-md-down">Phisit Liammukda &nbsp;<i class="fa fa-angle-down"></i></span> </a> --}}
                    <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i><span class="hidden-md-down">Phisit Liammukda &nbsp;<i class="fa fa-angle-down"></i></span> </a>
                    <div class="dropdown-menu dropdown-menu-right animated flipInY">
                        <ul class="dropdown-user">
                            <li>
                                <div class="dw-user-box">
                                    {{-- <div class="u-img"><img src="../../../../assets/images/users/1.jpg" alt="user"></div> --}}
                                    <div class="u-img"><i class="fas fa-user"></i></div>
                                    <div class="u-text">
                                        <h4>Phisit Liammukda</h4>
                                        <p class="text-muted">phisit.liammukda@gmail.com</p><a href="../../../pages/general/profile" class="btn btn-rounded btn-danger btn-sm">ดูโปรไฟล์</a></div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="../../../pages/general/profile"><i class="ti-user"></i>&nbsp; จัดการโปรไฟล์ธุรกิจ</a></li>
                            <!-- <li><a href="#"><i class="ti-wallet"></i>&nbsp; My Balance</a></li> -->
                            <!-- <li><a href="#"><i class="ti-email"></i>&nbsp; Inbox</a></li> -->
                            <!-- <li role="separator" class="divider"></li> -->
                            <!-- <li><a href="#"><i class="ti-settings"></i>&nbsp; Account Setting</a></li> -->
                            <li role="separator" class="divider"></li>
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off"></i>
                                    <span>ออกจากระบบ</span>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- ============================================================== -->
<!-- End Topbar header -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-chart-pie"></i><span class="hide-menu">รายงาน</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('dashboard') }}">ภาพรวม</a></li>
                    </ul>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-mobile"></i><span class="hide-menu">อุปกรณ์<span class="label label-rounded label-danger">4</span></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="../../../pages/general-dashboard/clients">ภาพรวม</a></li>
                        <li><a href="../../../pages/general-dashboard/clients">ภาพรวม</a></li>
                    </ul>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-map-marked-alt"></i><span class="hide-menu">พิกัด / หน้างาน<span class="label label-rounded label-danger">4</span></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="../../../pages/general-dashboard/clients">ภาพรวม</a></li>
                        <li><a href="../../../pages/general-dashboard/clients">ภาพรวม</a></li>
                    </ul>
                </li>

                @if(Auth::user()->role_id != null)
                    @if(Auth::user()->role->sell != 'not' && Auth::user()->role->buy != 'not' && Auth::user()->role->brand != 'not'  && Auth::user()->role->product != 'not' && Auth::user()->role->customer != 'not' && Auth::user()->role->finance != 'not' && Auth::user()->role->setting != 'not')

                            <li class="nav-small-cap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; POS</li>

                        @if(Auth::user()->role->sell != 'not')
                            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-shopping-basket"></i><span class="hide-menu">รายการขาย</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="{{ route('sell-record.create') }}">สร้างรายการขาย</a></li>
                                    <li><a href="{{ route('sell-record.index') }}">ดูรายการขาย</a></li>
                                </ul>
                            </li>
                        @endif

                        @if(Auth::user()->role->buy != 'not')
                            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-dolly"></i><span class="hide-menu">รายการซื้อ</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="{{ route('buy-record.create') }}">สร้างรายการซื้อ</a></li>
                                    <li><a href="{{ route('buy-record.index') }}">ดูรายการซื้อ</a></li>
                                </ul>
                            </li>
                        @endif


                        @if(Auth::user()->role->brand != 'not'  && Auth::user()->role->product != 'not')
                            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-barcode"></i><span class="hide-menu">สินค้า</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="{{ route('product.index') }}">สินค้า</a></li>
                                    <li><a href="{{ route('brand.index') }}">แบรนด์</a></li>
                                    <li><a href="{{ route('category.index') }}">หมวดหมู่</a></li>
                                </ul>
                            </li>
                        @endif

                        @if(Auth::user()->role->customer != 'not')
                            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-user-friends"></i><span class="hide-menu">ลูกค้า/คู้ค้า</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="{{ route('customer.index') }}">รายชื่อลูกค้า</a></li>
                                    <li><a href="{{ route('partner.index') }}">รายชื่อคู่ค้า</a></li>
                                </ul>
                            </li>
                        @endif


                        @if(Auth::user()->role->finance != 'not')
                            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-wallet"></i><span class="hide-menu">การเงิน</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="{{ route('finance.income.index') }}">รายรับ</a></li>
                                    <li><a href="{{ route('finance.cost.index') }}">รายจ่าย</a></li>
                                </ul>
                            </li>
                        @endif

                        @if(Auth::user()->role->setting != 'not')
                            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-cogs"></i><span class="hide-menu">ตั้งค่า</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="{{ route('setting.business.edit') }}">ข้อมูลธุรกิจ</a></li>
                                    <li><a href="{{ route('setting.user.index') }}">ผู้ใช้งาน</a></li>
                                    <li><a href="{{ route('setting.role.index') }}">สิทธิ์การใช้งาน</a></li>
                                </ul>
                            </li>
                        @endif

                    @endif
                @else

                    <li class="nav-small-cap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; POS</li>

                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-shopping-basket"></i><span class="hide-menu">รายการขาย</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="{{ route('sell-record.create') }}">สร้างรายการขาย</a></li>
                            <li><a href="{{ route('sell-record.index') }}">ดูรายการขาย</a></li>
                        </ul>
                    </li>

                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-dolly"></i><span class="hide-menu">รายการซื้อ</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="{{ route('buy-record.create') }}">สร้างรายการซื้อ</a></li>
                            <li><a href="{{ route('buy-record.index') }}">ดูรายการซื้อ</a></li>
                        </ul>
                    </li>

                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-barcode"></i><span class="hide-menu">สินค้า</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="{{ route('product.index') }}">สินค้า</a></li>
                            <li><a href="{{ route('brand.index') }}">แบรนด์</a></li>
                            <li><a href="{{ route('category.index') }}">หมวดหมู่</a></li>
                        </ul>
                    </li>

                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-user-friends"></i><span class="hide-menu">ลูกค้า/คู้ค้า</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="{{ route('customer.index') }}">รายชื่อลูกค้า</a></li>
                            <li><a href="{{ route('partner.index') }}">รายชื่อคู่ค้า</a></li>
                        </ul>
                    </li>

                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-wallet"></i><span class="hide-menu">การเงิน</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="{{ route('finance.income.index') }}">รายรับ</a></li>
                            <li><a href="{{ route('finance.cost.index') }}">รายจ่าย</a></li>
                        </ul>
                    </li>

                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-cogs"></i><span class="hide-menu">ตั้งค่า</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="{{ route('setting.business.edit') }}">ข้อมูลธุรกิจ</a></li>
                            <li><a href="{{ route('setting.user.index') }}">ผู้ใช้งาน</a></li>
                            <li><a href="{{ route('setting.role.index') }}">สิทธิ์การใช้งาน</a></li>
                        </ul>
                    </li>

                @endif

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->

