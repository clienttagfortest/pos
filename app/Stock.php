<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $connection = 'mysql';
    protected $table = 'stocks';
    protected $fillable = ['product_code', 'purchase_price', 'amount', 'company_id'];
}
