<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BuyRecord;


class DateRangeController extends Controller
{
    public function index(Request $request)
    {
        if(request()->ajax())
        {
            if(!empty($request->from_date))
            {
                $data = BuyRecord::whereBetween('date',array($request->from_date,$request->to_date))
                ->get();
            }
            else
            {
                $data = BuyRecord::get();
               }
               return datatables()->of($data)->make(true);
        }
        return view('buy-record.index');
    }
}

?>
