<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckSellRecord
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(Auth::user()->role_id);
        if (Auth::user()->role_id == null) {
            return $next($request);
        } else {
            $user_role = Auth::user()->role->sell;
            if ($user_role == 'not') {
                return abort(404);
            } else if($user_role == 'read') {
                return redirect()->route('sell-record.index');
            } else {
                return $next($request);
            }
        }

            
            

    }
}
