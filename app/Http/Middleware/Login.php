<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Login
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        // dd($user);
        if ($user->status == true) {
            if ($user->company_id == null) {
                return redirect()->route('setting.business.create');
            }
            return $next($request);
        } else  {
            Auth::logout();
            return redirect('/login');
        }
            
    }
}
