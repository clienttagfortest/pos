<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $connection = 'mysql';
    protected $table = 'products';
    protected $fillable = ['product_code', 'name', 'category_id', 'purchase_price', 'sale_price', 'unit_text', 'tag', 'company_id', 'brand_id', 'amount'];

    public function stock()
    {
    	return $this->hasMany('App\Stock');
    }

    public function brand()
    {
    	return $this->belongsTo('App\Brand');
    }
}
