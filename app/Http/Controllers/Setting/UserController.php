<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Hash;

use App\User;
use App\Role;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'login', 'CheckSetting']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_auth = Auth::user();

        $users = User::where('company_id', $user_auth->company_id)->where('id', '!=', $user_auth->id)->where('role_id', '!=', null)->get();
        return view('setting.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_auth = Auth::user();

        $roles = Role::where('company_id', $user_auth->company_id)->pluck('name', 'id');
        return view('setting.user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $input = $request->input();
        $input['company_id'] = $user->company_id;
        $input['password'] = Hash::make($input['password']);
        User::create($input);
        return redirect()->route('setting.user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_auth = Auth::user();

        $roles = Role::where('company_id', $user_auth->company_id)->pluck('name', 'id');
        $user = User::findOrFail($id);

        return view('setting.user.edit', compact('roles', 'user'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_auth = Auth::user();

        $input = $request->input();
        $user = User::findOrFail($id);
        $user->update($input);
        return redirect()->route('setting.user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
