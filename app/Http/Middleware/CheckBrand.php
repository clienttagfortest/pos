<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckBrand
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->role_id == null) {
            return $next($request);
        } else {
            $user_role = Auth::user()->role->brand;
            if ($user_role == 'not') {
                return abort(404);
            } else if($user_role == 'read') {
                return redirect()->route('brand.index');
            } else {
                return $next($request);
            }
        }
    }
}
