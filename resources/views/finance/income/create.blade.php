@extends('layouts.master')
@section('title', 'Income')
@section('content')

    <div class="row page-titles mb-1">
        <div class="col-md-5 align-self-center">
            <h2 class="text-themecolor">สร้างรายรับ</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
                <li class="breadcrumb-item"><a href="{{ route('finance.income.index') }}">รายรับ</a></li>
                <li class="breadcrumb-item active">สร้างรายรับ</li>
            </ol>
        </div>
    </div>

    <div class="row">

        <!-- col-6 -->
        <div class="col-md-6">
            <hr>
            <div class="form-group">
                <div class="row">
                    <label for="" class="col-md-3 col-3 text-right">ประเภท</label>
                    <div class="col-md-8 col-9">
                        <select class="form-control" id="financesubtype">
                            <option value="0">ทั่วไป</option>
                            <option value="1">พิเศษ</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <label for="" class="col-md-3 col-3 text-right">วันที่ทำรายการ <span style="color: red;">*</span></label>
                    <div class="col-md-8 col-9">
                        <div class="input-group date form_date" data-date="" data-date-format="d/m/yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                            <input class="form-control" type="text" name="financedate" value="" id="financedate" onkeyup="setNormalTextbox(this.id);" readonly="">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <label for="" class="col-md-3 col-3 text-right">ประเภทภาษี</label>
                    <div class="col-md-8 col-9">
                        <select class="form-control" id="vattypeid" onchange="changeVatType()">
                            <option value="1:0:1" selected="">ไม่มีภาษี</option>
                            <option value="2:7:0">แยกภาษีมูลค่าเพิ่ม 7%</option>
                            <option value="3:7:1">รวมภาษีมูลค่าเพิ่ม 7%</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>
        <!-- col-6 -->

        <!-- col-6 -->
        <div class="col-md-6">
            <hr>

            <div class="form-group">
                <div class="row">
                    <label for="" class="col-md-3 col-3 text-right">แท๊คโค็ด</label>
                    <div class="col-md-8 col-9">
                        <input type="text" class="form-control" id="customercode" maxlength="128" value="">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <label for="" class="col-md-3 col-3 text-right">เบอร์โทรศัพท์</label>
                    <div class="col-md-8 col-9">
                        <input type="text" class="form-control" id="customerphone" maxlength="64" value="">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <label for="" class="col-md-3 col-3 text-right">อีเมล</label>
                    <div class="col-md-8 col-9">
                        <input type="text" class="form-control" id="customeremail" maxlength="128" value="">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="togglebutton row">
                    <label for="" class="col-md-3 col-3 text-right"></label>
                    <div class="col-md-8 col-9">
                        <label>
                            <input type="checkbox" id="chkArea">
                            <span class="toggle"></span>
                            เพิ่มลูกค้า
                        </label>
                    </div>
                </div>
            </div>

        </div>
        <!-- / .col-6 -->

        <div class="col-md-12">

            <div id="showarea" style="display:none;">
                <div class="row">

                    <div class="col-md-6">

                        <div class="form-group">
                            <div class="row">
                                <label for="" class="col-md-3 col-3 text-right">ชื่อ</label>
                                <div class="col-md-8 col-9">
                                    <input type="text" class="form-control" id="customerphone" maxlength="64" value="">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label for="" class="col-md-3 col-3 text-right">นามสกุล</label>
                                <div class="col-md-8 col-9">
                                    <input type="text" class="form-control" id="customerphone" maxlength="64" value="">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label for="" class="col-md-3 col-3 text-right">เลขประจําตัวผู้เสียภาษี</label>
                                <div class="col-md-8 col-9">
                                    <input type="text" class="form-control" id="customerphone" maxlength="64" value="">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-6">

                        <div class="form-group">
                            <div class="row">
                                <label for="" class="col-md-3 col-3 text-right">ที่อยู่</label>
                                <div class="col-md-8 col-9">
                                    <input type="text" class="form-control" id="customerphone" maxlength="64" value="">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label for="" class="col-md-3 col-3 text-right">จังหวัด</label>
                                <div class="col-md-8 col-9">
                                    <select class="form-control" id="vattypeid" onchange="changeVatType()">
                                        <option value="1:0:1" selected="">เลือกจังหวัด</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label for="" class="col-md-3 col-3 text-right">เขต/อำเภอ</label>
                                <div class="col-md-8 col-9">
                                    <select class="form-control" id="vattypeid" onchange="changeVatType()">
                                        <option value="1:0:1" selected="">เลือกเขต/อำเภอ</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label for="" class="col-md-3 col-3 text-right">แขวง/ตำบล</label>
                                <div class="col-md-8 col-9">
                                    <select class="form-control" id="vattypeid" onchange="changeVatType()">
                                        <option value="1:0:1" selected="">เลือกแขวง/ตำบล</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label for="" class="col-md-3 col-3 text-right">รหัสไปรษณีย์</label>
                                <div class="col-md-4 col-9">
                                    <input type="text" class="form-control" id="customerphone" maxlength="64" value="">
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

            <div id="hidearea">
            </div>

        </div>
        <!-- col-md-6 -->


    </div>


    <div class="row">

        <div class="col-md-12">
            <table class="table table-hover" id="worked">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Telephone</th>
                        <th>
                            <button type="button" class="btn btn-info add-row">+</button></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <input class="form-control" type="text"></td>
                        <td>
                            <input class="form-control" type="text"></td>
                        <td>
                            <input class="form-control" type="text"></td>
                        <td>
                            <input class="form-control" type="text"></td>
                        <td>
                            <button type="button" class="btn btn-danger delete-row">-</button></td>
                    </tr>

                </tbody>
            </table>
        </div>

    </div>

    <!-- / .total summary -->
    <div class="total-summary-section row">

        <div class="col-md-6">
        </div>

        <div class="col-md-6">
            <div class="total-summary">
                <div class="row">
                    <div class="col-6 text-right">
                        <p>มูลค่ารวมก่อนภาษี</p>
                    </div>
                    <div class="col-6">
                        <p>
                            <span id="amount2text">0.00</span>
                            <input type="hidden" style="width: 150px;" id="amount2" value="0.00">
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6 text-right">
                        <p>ภาษีมูลค่าเพิ่ม (7%)</p>
                    </div>
                    <div class="col-6">
                        <p><span id="vatamounttext">0.00</span>
                            <input type="hidden" class="form-control" id="vatamount" onblur="autocalculate()" maxlength="32" onfocus="removeComma(this.id);" value="0.00">
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6 text-right">
                        <p>มูลค่ารวมสุทธิ</p>
                    </div>
                    <div class="col-6">
                        <p><span id="amounttext">0.00</span>
                            <input type="hidden" id="amount" value="0.00">
                        </p>
                    </div>
                </div>

                <div class="row">
                    <input type="hidden" id="paymentamount" maxlength="32" value="0.00"><input type="hidden" id="paymentname" maxlength="32" value=""><input type="hidden" id="paymentdatetimestatus" value="0"><input type="hidden" id="paymentdatetime" value="">
                </div>
            </div>
        </div>

    </div>
    <!-- / .total summary -->

    <div class="row">
        <div class="col-md-12">
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group whtarea" style="display:none;">
                        <label for="" class="col-sm-3 col-md-5 control-label" style="text-align:right;"><b>ยอดชำระรวม</b></label>
                        <div class="col-sm-9 col-md-6">
                            <p><span id="paymentamounttext">0.00</span></p>
                        </div>
                    </div>
                    <ul class="dropdown-menu sub-nav">
                        <li><a href="">บันทึก</a></li>
                        <li><a href="">บันทึก + สร้างใหม่</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12">
        <button type="button" class="btn btn-primary float-right">บันทึก</button>
    </div>

@stop
