@extends('layouts.master')
@section('title', 'Brand')
@section('content')

    <div class="row page-titles mb-1">
        <div class="col-md-5 align-self-center">
            <h2 class="text-themecolor">เพิ่มแบรนด์</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
                <li class="breadcrumb-item"><a href="{{ route('brand.index') }}">แบรนด์</a></li>
                <li class="breadcrumb-item active">เพิ่มแบรนด์</li>
            </ol>
        </div>
    </div>

    <!-- Default box -->
    <div class="card">

        <div class="card-body">

        {!! Form::open(['route' => 'brand.store', 'method' => 'post', 'files' => true]) !!}

        <div class="form-group">
            {!! Form::label('name', 'ชื่อแบรนด์') !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('category_id', 'หมวดหมู่') !!}
            {!! Form::select('category_id', $categories, null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('image', 'รูป') !!}
            {!! Form::file('image', ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
        </div>

        {!! Form::close() !!}

        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

@stop
