@extends('layouts.master')
@section('title', 'Income')
@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center mb-2">
            <h2 class="text-themecolor">รายรับ</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
                <li class="breadcrumb-item active">รายรับ</li>
            </ol>
        </div>
        <div class="col-sm-7 align-self-center">
            <a href="{{ route('finance.income.create') }}" class="btn btn-info float-right" >+ เพิ่มรายรับ</a>
        </div>
    </div>

        <!-- Default box -->
        <div class="card">

            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive-sm">
                    <table id="dataTable" class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">ชื่อผู้ใช้งาน</th>
                                <th scope="col">วันที่สมัคร</th>
                                <th scope="col">เข้าใช้งานล่าสุด</th>
                                <th scope="col">สิทธิ์การใช้งาน</th>
                                <th scope="col">สถานะ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>awd</td>
                                <td>awd</td>
                                <td>awd</td>
                                <td>awd</td>
                                <td>awd</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">เพิ่มผู้ใช้งาน</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="create.php" method="post" enctype="multipart/form-data">

                                    <div class="form-group row">
                                        <label for="tagCode" class="col-sm-4 col-form-label text-sm-right">แท๊คโค้ด</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="productName" value="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="productName" class="col-sm-4 col-form-label text-sm-right">อีเมล<span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="productName" value="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="productName" class="col-sm-4 col-form-label text-sm-right">รหัสผ่าน<span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="productName" value="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="productName" class="col-sm-4 col-form-label text-sm-right">ยืนยันรหัสผ่าน<span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="productName" value="">
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
                                <button type="button" class="btn btn-primary">บันทึก</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.Modal -->

            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

@stop
