@extends('layouts.master')
@section('title', 'Customer')
@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center mb-2">
            <h2 class="text-themecolor">เพิ่มลูกค้า</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
                <li class="breadcrumb-item"><a href="{{ route('customer.index') }}">รายชื่อลูกค้า</a></li>
                <li class="breadcrumb-item active">เพิ่มลูกค้า</li>
            </ol>
        </div>
    </div>

    <div class="card card-form">
        {!! Form::open(['route' => 'customer.store', 'method' => 'post']) !!}

        <div class="form-group">
            {!! Form::label('name', 'ชื่อ') !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('tel', 'เบอร์มือถือ') !!}
            {!! Form::number('tel', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('email', 'อีเมล') !!}
            {!! Form::email('email', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('address', 'ที่อยู่') !!}
            {!! Form::textarea('address', null, ['class' => 'form-control', 'rows' => 3, 'required']) !!}
        </div>


        <div class="form-group">
            {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
        </div>

        {!! Form::close() !!}
    </div>

@stop
