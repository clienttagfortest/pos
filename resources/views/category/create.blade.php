@extends('layouts.master')
@section('title', 'Category')
@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center mb-2">
            <h2 class="text-themecolor">เพิ่มหมวดหมู่</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">หน้าหลัก</a></li>
                <li class="breadcrumb-item"><a href="{{ route('category.index') }}">หมวดหมู่</a></li>
                <li class="breadcrumb-item active">เพิ่มหมวดหมู่</li>
            </ol>
        </div>
    </div>

    <!-- Default box -->
    <div class="card">
        <div class="card-body">

        {!! Form::open(['route' => 'category.store', 'method' => 'post']) !!}

        <div class="form-group">
            {!! Form::label('name', 'ชื่อหมวดหมู่') !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
        </div>

        {!! Form::close() !!}

        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
@stop
