@extends('layouts.master')
@section('title', 'Dashboard')
@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h2 class="text-themecolor">ภาพรวม</h2>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Info box -->
    <!-- ============================================================== -->
    <div class="card-group">

        <div class="card shadow-none">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <h3>{{ $sum_sale }}</h3>
                        <h6 class="card-subtitle">ยอดขายวันนี้</h6>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="card shadow-none">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <h3>{{ $sum_sale }}</h3>
                        <h6 class="card-subtitle">ยอดขายประจำเดือน</h6>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="card shadow-none">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <h3>{{ $sum_sale }}</h3>
                        <h6 class="card-subtitle">ยอดขายทั้งหมด</h6>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="card shadow-none">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <h3>{{ $profit }}</h3>
                        <h6 class="card-subtitle">กำไร</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Info box -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Sales Chart and browser state-->
    <!-- ============================================================== -->
    <div class="card-group">
        <div class="card shadow-none">
            <div class="card-body">

                <div class="row">
                    <!-- Column -->
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block">
                                    <div>
                                        <h5 class="card-title m-b-0">Sales Chart</h5>
                                    </div>
                                    <div class="ml-auto">
                                        <ul class="list-inline text-center font-12">
                                            <li><i class="fa fa-circle text-success"></i> SITE A</li>
                                            <li><i class="fa fa-circle text-info"></i> SITE B</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="" id="sales-chart" style="height: 465px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card shadow-none">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card o-income">
                            <div class="card-body">
                                <div class="d-flex m-b-30 no-block">
                                    <h5 class="card-title m-b-0 align-self-center">Our Income</h5>
                                    <div class="ml-auto">
                                        <select class="custom-select b-0">
                                            <option selected="">January</option>
                                            <option value="1">February</option>
                                            <option value="2">March</option>
                                            <option value="3">April</option>
                                        </select>
                                    </div>
                                </div>
                                <div id="income" style="height:360px; width:100%;"></div>
                                <ul class="list-inline m-t-30 text-center font-12">
                                    <li><i class="fa fa-circle text-success"></i> Growth</li>
                                    <li><i class="fa fa-circle text-info"></i> Net</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
            </div>
        </div>

    </div>

    <!-- ============================================================== -->
    <!-- End Sales Chart -->
    <!-- ============================================================== -->
    <div class="card-group">

        <div class="card shadow-none">
            <div class="card-body">
                <div class="row">

                    <div class="col-md-12">

                        <div class="table-header">
                            <h3>รายการขายล่าสุด</h3>
                        </div>
                        <table class="table table-sm">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>รายการ</th>
                                    <th>มูลค่า</th>
                                    <th>วันที่ทำรายการ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sales as $key => $sale)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $sale->sell_record_code }}</td>
                                    <td>{{ $sale->result_price }}</td>
                                    <td>{{ $sale->date }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                    {{-- <div class="card-body">
                        <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <a class="btn btn-warning" href="{{ route('export') }}">Export Rec Data</a>
                        </form>
                    </div> --}}

                </div>
            </div>
        </div>

        <div class="card shadow-none">
            <div class="card-body">
                <div class="row">

                <div class="col-md-12">

                    <div class="table-header">
                        <h3>รายการซื้อล่าสุด</h3>
                    </div>
                    <table class="table table-sm">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>รายการ</th>
                                <th>มูลค่า</th>
                                <th>วันที่ทำรายการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($buys as $key => $buy)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $buy->buy_record_code }}</td>
                                <td>{{ $buy->result_price }}</td>
                                <td>{{ $buy->date }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>

            </div>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
@stop
