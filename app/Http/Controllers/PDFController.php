<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class PDFController extends Controller
{
    public function generatePDF()
    {
        $data = ['title' => 'Invoice'];
        $pdf = PDF::loadView('myPDF', $data);

        return $pdf->download('Test.pdf');
        return view('show.index');
    }
}
